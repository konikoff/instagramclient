Run docker container with **MariaDB**:
~~~~
docker run --name mariadb -e MYSQL_ROOT_PASSWORD=admin -e MYSQL_DATABASE=instabot -p 3306:3306 -d mariadb:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
~~~~