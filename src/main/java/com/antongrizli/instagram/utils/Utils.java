package com.antongrizli.instagram.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class Utils {

    public static void randomWaiting() throws InterruptedException {
        // generate a random integer from 1.5 sec to 8 sec
        int x = getRandomIntegerBetweenRange(1500, 8000);

        TimeUnit.MILLISECONDS.sleep(x);

        log.debug("Waiting for: " + x + " ms");
    }

    private static int getRandomIntegerBetweenRange(int min, int max) {
        return (int) ((Math.random() * ((max - min) + 1)) + min);
    }
}
