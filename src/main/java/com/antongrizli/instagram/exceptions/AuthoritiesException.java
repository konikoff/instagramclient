package com.antongrizli.instagram.exceptions;

public class AuthoritiesException extends RuntimeException {

    public AuthoritiesException() {
        super("Authorities exception!");
    }

    public AuthoritiesException(String message) {
        super(message);
    }
}
