package com.antongrizli.instagram.exceptions;

public class EmailExistsException extends RuntimeException {

    public EmailExistsException() {
        super("This email is already exists!");
    }

    public EmailExistsException(String message) {
        super(message);
    }
}
