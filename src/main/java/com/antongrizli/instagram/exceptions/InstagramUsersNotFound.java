package com.antongrizli.instagram.exceptions;

public class InstagramUsersNotFound extends RuntimeException {

    public InstagramUsersNotFound() {
        super("Unfortunately we didn't find users!");
    }
}
