package com.antongrizli.instagram.configurations;

import com.antongrizli.instagram.configurations.security.CustomLoginSuccessHandler;
import com.antongrizli.instagram.configurations.security.CustomLogoutSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfigs extends WebSecurityConfigurerAdapter {

    private final UserDetailsService customUserDetailsService;

    public SecurityConfigs(UserDetailsService customUserDetailsService) {
        this.customUserDetailsService = customUserDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         http.csrf().disable().authorizeRequests().anyRequest().permitAll();
         http
         .authorizeRequests()
         .antMatchers("/admin/**").hasRole("ADMIN")
         .antMatchers("/h2-console/**", "/webjars/**", "/").permitAll()
         .antMatchers("/anonymous*").anonymous()
         //.antMatchers("/auth/username*").permitAll()
         .anyRequest().authenticated()
         .and()
         .formLogin()
         .loginPage("/auth/sign-in")
         .usernameParameter("username")
         .passwordParameter("password")
         .loginSuccessHandler(loginSuccessHandler())
         .permitAll()
         .and().csrf().ignoringAntMatchers("/h2-console/**")//don't apply CSRF protection to /h2-console
         .and().headers().frameOptions().sameOrigin();//allow use of frame to same origin urls;
         */
        http
                .authorizeRequests()
                .antMatchers("/", "/index", "/home", "/h2-console/**", "/webjars/**", "/auth/sign-up").permitAll()
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                        .loginPage("/auth/sign-in")
                        .usernameParameter("username")
                        .passwordParameter("password")
                        .successHandler(loginSuccessHandler())
                        .permitAll()
                .and()
                    .logout()
                        .logoutUrl("/auth/logout")
                        .logoutSuccessUrl("/")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                        .logoutSuccessHandler(logoutSuccessHandler());

    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(customUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public AuthenticationSuccessHandler loginSuccessHandler() {
        //SimpleUrlAuthenticationSuccessHandler handler = new SimpleUrlAuthenticationSuccessHandler();
        //handler.setUseReferer(true);
        return new CustomLoginSuccessHandler();
    }

    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new CustomLogoutSuccessHandler();
    }
}

