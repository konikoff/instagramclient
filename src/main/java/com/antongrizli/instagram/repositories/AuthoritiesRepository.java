package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.Authorities;
import com.antongrizli.instagram.domain.enums.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuthoritiesRepository extends CrudRepository<Authorities, Long> {

    Optional<Authorities> findByRole(Role role);
}
