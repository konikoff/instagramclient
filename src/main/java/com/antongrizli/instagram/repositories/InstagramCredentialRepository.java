package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.InstagramCredentials;
import org.springframework.data.repository.CrudRepository;

public interface InstagramCredentialRepository extends CrudRepository<InstagramCredentials, Long> {
}
