package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.InstagramFollowingUser;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface InstagramFollowingUserRepository extends CrudRepository<InstagramFollowingUser, Long> {

    @Query(value = "select * from instagram_following_users where pk not in (select instagram_followed_users.pk from instagram_followed_users) and instagram_cookie_id = :instagram_cookie_id", nativeQuery = true)
    List<InstagramFollowingUser> findAllUnmutualUsers(@Param("instagram_cookie_id") Long id);

    @Query(value = "select * from instagram_following_users where pk in (select instagram_followed_users.pk from instagram_followed_users) and instagram_cookie_id = :instagram_cookie_id", nativeQuery = true)
    List<InstagramFollowingUser> findAllMutualUsers(@Param("instagram_cookie_id") Long id);

    @Transactional
    @Query(value = "delete from instagram_following_users where pk = :pk and instagram_cookie_id = :instagramCredentialsId", nativeQuery = true)
    void deleteInstagramFollowingUserByPkAndCAndInstagramCredentials(@Param("pk") Long pk, @Param("instagramCredentialsId") Long instagramCredentialsId);

    Optional<List<InstagramFollowingUser>> findAllByInstagramCredentials_Id(Long credentialId);

    @Transactional
    @Modifying
    @Query(value = "insert instagram_following_users(full_name, pk, profile_pic_url, sync_date, username, instagram_cookie_id) values (:full_name, :pk, :profile_pic_url, :sync_date, :username, :instagram_cookie_id) ON DUPLICATE KEY UPDATE sync_date = :sync_date", nativeQuery = true)
    void insertFollowingUser(@Param("full_name") String fullName,
                             @Param("pk") Long pk,
                             @Param("profile_pic_url") String profilePicUrl,
                             @Param("sync_date") Instant syncDate,
                             @Param("username") String username,
                             @Param("instagram_cookie_id") Long instagramCookieId);
}
