package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface InstagramInstanceRepository extends CrudRepository<InstagramCredentials, Long> {

    Optional<List<InstagramCredentials>> findAllByUser_Id(Long userId);

    Optional<InstagramCredentials> findFirstByUsernameAndUser(String instagramUsername, User user);

    @Query(value = "select id from InstagramCredentials where username = :username and user = :user")
    Optional<Long> getInstagramCredentialIdByUsernameAndUser(@Param("username") String username, @Param("user") User user);
}
