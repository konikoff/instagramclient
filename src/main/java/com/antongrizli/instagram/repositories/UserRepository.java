package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    @Query(value = "SELECT u FROM User u LEFT JOIN FETCH u.instagramCredentials WHERE u.email = :email")
    Optional<User> findByEmail(@Param("email") String email);
}
