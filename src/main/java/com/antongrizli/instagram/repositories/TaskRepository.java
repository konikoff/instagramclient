package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task, Long> {

    Optional<List<Task>> findAllByInstagramCredentials_Id(Long credentialID);

    Optional<Task> findByIdAndInstagramCredentials(Long id, InstagramCredentials instagramCredentials);

    Optional<List<Task>> findAllByStatus(TaskStatus status);

    void deleteByIdAndInstagramCredentials(Long id, InstagramCredentials instagramCredentials);
}
