package com.antongrizli.instagram.repositories;

import com.antongrizli.instagram.domain.InstagramFollowedUser;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface InstagramFollowedUserRepository extends CrudRepository<InstagramFollowedUser, Long> {

    Optional<List<InstagramFollowedUser>> findAllByInstagramCredentials_Id(Long credentialId);

    @Transactional
    @Modifying
    @Query(value = "insert instagram_followed_users (full_name, pk, profile_pic_url, sync_date, username, instagram_cookie_id) values (:full_name, :pk, :profile_pic_url, :sync_date, :username, :instagram_cookie_id) ON DUPLICATE KEY UPDATE sync_date = :sync_date", nativeQuery = true)
    void insertFollowedUser(@Param("full_name") String fullName,
                            @Param("pk") Long pk,
                            @Param("profile_pic_url") String profilePicUrl,
                            @Param("sync_date") Instant syncDate,
                            @Param("username") String username,
                            @Param("instagram_cookie_id") Long instagramCookieId);
}
