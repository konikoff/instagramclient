package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.RegistrationFormCommand;
import com.antongrizli.instagram.converters.RegistrationFormToUser;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.exceptions.EmailExistsException;
import com.antongrizli.instagram.repositories.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RegistrationFormToUser registrationFormToUser;


    public UserServiceImpl(UserRepository userRepository,
                           RegistrationFormToUser registrationFormToUser) {
        this.userRepository = userRepository;
        this.registrationFormToUser = registrationFormToUser;
    }

    @Override
    @Transactional
    public void save(User user) {
        if (!this.userRepository.findByEmail(user.getEmail()).isPresent()) {
            userRepository.save(user);
        }
    }

    @Override
    @Transactional
    public User registerNewUserAccount(RegistrationFormCommand form) {
        if (emailExists(form.getEmail())) {
            throw new EmailExistsException("There is an account with that email address:" + form.getEmail());
        }

        User user = registrationFormToUser.convert(form);

        return userRepository.save(user);
    }

    private boolean emailExists(String email) {
        return this.userRepository.findByEmail(email).isPresent();
    }

    @Override
    @Transactional
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    @Transactional
    public User findByLogin(String login) {
        return this.userRepository.findByEmail(login).orElse(new User());
    }

    @Override
    @Transactional
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    @Transactional
    public void deleteAll() {
        userRepository.deleteAll();
    }

}
