package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import com.antongrizli.instagram.repositories.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    @Transactional
    public List<Task> getAllTasksByInstagramLogin(UserCommand userCommand, String instagramLogin) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();

        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                Long credentialId = cookie.getId();

                List<Task> taskList = taskRepository.findAllByInstagramCredentials_Id(credentialId).orElse(new ArrayList<>());

                log.debug("Size of tasks: " + taskList.size() + ", for user: " + instagramLogin);
                return taskList;
            }
        }

        throw new RuntimeException("The user: " + instagramLogin + " doesn't belong to: " + userCommand.getLogin());
    }

    @Override
    @Transactional
    public List<Task> getAllTasks() {
        List<Task> taskList = new ArrayList<>();
        taskRepository.findAll().forEach(taskList::add);
        return taskList;
    }

    @Override
    @Transactional
    public void saveAllTasks(List<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Override
    @Transactional
    public List<Task> getWaitingTasks() {
        return taskRepository.findAllByStatus(TaskStatus.WAITING).orElse(new ArrayList<>());
    }

    @Override
    @Transactional
    public List<Task> getRunningTasks() {
        return taskRepository.findAllByStatus(TaskStatus.RUNNING).orElse(new ArrayList<>());
    }


    @Override
    @Transactional
    public Task save(Task task) {
        return this.taskRepository.save(task);
    }

    @Override
    @Transactional
    public Task findTaskByIdAndCredential(Long id, InstagramCredentials credential) {
        return taskRepository.findByIdAndInstagramCredentials(id, credential).orElse(new Task());
    }

    @Override
    @Transactional
    public void deleteTaskByIdAndCredential(Long id, InstagramCredentials credential) {
        taskRepository.deleteByIdAndInstagramCredentials(id, credential);
    }

    @Override
    public Boolean isTaskExist(Task task) {
        return taskRepository.existsById(task.getId());
    }
}
