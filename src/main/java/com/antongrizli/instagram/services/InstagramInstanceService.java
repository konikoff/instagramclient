package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramInstance;
import com.antongrizli.instagram.domain.User;
import org.brunocvcunha.instagram4j.Instagram4j;

import java.util.List;

public interface InstagramInstanceService {

    List<InstagramInstance> getAllInstagramInstances();

    List<InstagramInstance> getAllByUserId(UserCommand userCommand);

    /**
     * Save CookieStore for Instagram4j
     *
     * @param instagram
     * @return
     */
    InstagramCredentials save(Instagram4j instagram, UserCommand userCommand);

    Instagram4j getInstagramInstanceById(Long id);

    Instagram4j getInstagramInstanceByInstagramUsernameAndUser(String instagramUsername, User user);

    Long getInstagramInstanceIdByInstagramUsernameAndUser(String instagramUsername, User user);

    Instagram4j getInstagram4j(InstagramCredentials instagramCredentials);

    void delete(String id);
}
