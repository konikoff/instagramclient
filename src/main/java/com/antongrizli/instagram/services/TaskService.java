package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.Task;

import java.util.List;

public interface TaskService {
    List<Task> getAllTasksByInstagramLogin(UserCommand savedUserCommand, String instagramLogin);

    List<Task> getAllTasks();

    void saveAllTasks(List<Task> tasks);

    List<Task> getWaitingTasks();

    List<Task> getRunningTasks();

    Task save(Task task);

    Task findTaskByIdAndCredential(Long id, InstagramCredentials credential);

    void deleteTaskByIdAndCredential(Long id, InstagramCredentials credential);

    Boolean isTaskExist(Task task);
}
