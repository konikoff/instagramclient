package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.InstagramCookieConvertToInstance;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramInstance;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.exceptions.InstagramUsersNotFound;
import com.antongrizli.instagram.repositories.InstagramInstanceRepository;
import com.antongrizli.instagram.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.client.CookieStore;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class InstagramInstanceServiceImpl implements InstagramInstanceService {
    private final InstagramInstanceRepository instanceRepository;
    private final InstagramCookieConvertToInstance convertToInstance;
    private final UserRepository userRepository;

    public InstagramInstanceServiceImpl(InstagramInstanceRepository instanceRepository,
                                        InstagramCookieConvertToInstance convertToInstance,
                                        UserRepository userRepository) {
        this.instanceRepository = instanceRepository;
        this.convertToInstance = convertToInstance;
        this.userRepository = userRepository;
    }


    @Override
    @Transactional
    public List<InstagramInstance> getAllInstagramInstances() {
        List<InstagramInstance> instanceList = new ArrayList<>();
        instanceRepository.findAll().forEach(instagramCookie -> instanceList.add(convertToInstance.convert(instagramCookie)));
        return instanceList;
    }

    @Override
    @Transactional
    public List<InstagramInstance> getAllByUserId(UserCommand UserCommand) {
        List<InstagramInstance> instanceList = new ArrayList<>();
        Optional<List<InstagramCredentials>> instagramInstancesOptional = instanceRepository.findAllByUser_Id(UserCommand.getId());
        instagramInstancesOptional.ifPresent(instagramCookies -> instagramCookies.forEach(instagramCookie -> instanceList.add(convertToInstance.convert(instagramCookie))));
        return instanceList;
    }

    @Override
    @Transactional
    public InstagramCredentials save(Instagram4j instagram, UserCommand userCommand) {
        CookieStore cookieStore = instagram.getCookieStore();

        ObjectOutput out;
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            out = new ObjectOutputStream(bos);
            out.writeObject(cookieStore);
            out.flush();

            Byte[] cookieStoreBytes = ArrayUtils.toObject(bos.toByteArray());
            User user = this.userRepository.findByEmail(userCommand.getLogin()).orElse(new User());

            //Saving instagram credentials
            InstagramCredentials instagramCredentials = new InstagramCredentials();
            instagramCredentials.setBytesCookieStore(cookieStoreBytes);
            instagramCredentials.setUsername(instagram.getUsername());
            instagramCredentials.setPassword(instagram.getPassword());
            instagramCredentials.setInstagramUserId(instagram.getUserId());
            instagramCredentials.setUuid(instagram.getUuid());

            user.addInstaCookie(instagramCredentials);
            this.userRepository.save(user);

            return instagramCredentials;


        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public Instagram4j getInstagramInstanceById(Long id) {
        InstagramCredentials instagramCredentials = instanceRepository.findById(id).orElse(null);
        return getInstagram4j(instagramCredentials);
    }

    @Override
    public Instagram4j getInstagramInstanceByInstagramUsernameAndUser(String instagramUsername, User user) {
        InstagramCredentials instagramCredentials = instanceRepository.findFirstByUsernameAndUser(instagramUsername, user).orElseThrow(InstagramUsersNotFound::new);
        return getInstagram4j(instagramCredentials);
    }

    @Override
    public Long getInstagramInstanceIdByInstagramUsernameAndUser(String instagramUsername, User user) {
        return instanceRepository.getInstagramCredentialIdByUsernameAndUser(instagramUsername, user).orElseThrow(InstagramUsersNotFound::new);
    }

    @Override
    @Transactional
    public Instagram4j getInstagram4j(InstagramCredentials instagramCredentials) {
        if (instagramCredentials != null) {
            try {
                ObjectInputStream ios = new ObjectInputStream(new ByteArrayInputStream(ArrayUtils.toPrimitive(instagramCredentials.getBytesCookieStore())));
                CookieStore cookieStore = (CookieStore) ios.readObject();

                Instagram4j instagram4j = Instagram4j.builder()
                        .username(instagramCredentials.getUsername())
                        .password(instagramCredentials.getPassword())
                        .userId(instagramCredentials.getInstagramUserId())
                        .uuid(instagramCredentials.getUuid())
                        .cookieStore(cookieStore).build();
                instagram4j.setup();

                return instagram4j;

            } catch (IOException | ClassNotFoundException e) {
                log.error(e.getMessage());
                throw new RuntimeException(e.getMessage());
            }
        } else {
            throw new RuntimeException("InstagramCredentials is null");
        }
    }

    @Override
    @Transactional
    public void delete(String id) {
        this.instanceRepository.deleteById(Long.valueOf(id));
    }
}
