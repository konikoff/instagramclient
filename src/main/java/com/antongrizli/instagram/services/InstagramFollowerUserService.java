package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.InstagramFollowerUser;
import com.antongrizli.instagram.domain.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface InstagramFollowerUserService {

    @Deprecated
    void saveFollowers(Long credentialId, List<? extends InstagramFollowerUser> instagramFollowedUsers);

    void syncFollowedUsers(String instagramId, UserCommand userCommand);

    void syncFollowingUsers(String instagramLogin, UserCommand userCommand);

    List<? extends InstagramFollowerUser> getFollowedUsers(String instagramId, UserCommand userCommand);

    List<? extends InstagramFollowerUser> getFollowingUsers(String instagramId, UserCommand userCommand);

    @Transactional
    void saveFollowedUsers(List<? extends InstagramFollowerUser> followerUsers);

    @Transactional
    void saveFollowingUsers(List<? extends InstagramFollowerUser> followerList);

    @Transactional
    List<? extends InstagramFollowerUser> getMutualUsers(String instagramLogin, User user);

    @Transactional
    List<? extends InstagramFollowerUser> getUnmutualUsers(String instagramLogin, User user);


    void unfollowInstagramUser(String instagramLogin, User user, Long pk);
}
