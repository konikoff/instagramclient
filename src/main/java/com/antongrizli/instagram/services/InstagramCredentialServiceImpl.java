package com.antongrizli.instagram.services;

import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramFollowedUser;
import com.antongrizli.instagram.repositories.InstagramCredentialRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class InstagramCredentialServiceImpl implements InstagramCredentialService {

    private final InstagramCredentialRepository instagramCredentialRepository;

    public InstagramCredentialServiceImpl(InstagramCredentialRepository instagramCredentialRepository) {
        this.instagramCredentialRepository = instagramCredentialRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<InstagramCredentials> getAllCredentials() {
        List<InstagramCredentials> credentialsList = new ArrayList<>();
        instagramCredentialRepository.findAll().forEach(credentialsList::add);
        return credentialsList;
    }

    @Override
    @Transactional
    public void save(InstagramCredentials credentials) {
        Long credentialId = credentials.getId();

        InstagramCredentials savedCredentials = instagramCredentialRepository.findById(credentialId).orElse(null);

        List<InstagramFollowedUser> savedFollowers = savedCredentials.getFollowers();

        List<InstagramFollowedUser> updateFollowers = new ArrayList<>();
        List<InstagramFollowedUser> unfollowed = new ArrayList<>();

        for (InstagramFollowedUser syncFollowedUser : credentials.getFollowers()) {
            boolean existingUser = false;
            for (InstagramFollowedUser savedUser : savedFollowers) {
                if (savedUser.getPk().equals(syncFollowedUser.getPk())) {
                    existingUser = true;
                }
            }

            if (!existingUser) {
                syncFollowedUser.setSyncDate(Instant.now());
                updateFollowers.add(syncFollowedUser);
            }

        }

        credentials.setFollowers(updateFollowers);

        instagramCredentialRepository.save(credentials);
    }

}
