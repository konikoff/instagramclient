package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.InstagramFeedResultCommand;
import com.antongrizli.instagram.commands.IntsagramJobCommand;
import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.InstagramCookieConvertToInstance;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramInstance;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.exceptions.InstagramUsersNotFound;
import com.antongrizli.instagram.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.*;
import org.brunocvcunha.instagram4j.requests.payload.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class InstagramServiceImpl implements InstagramService {

    private final InstagramInstanceService instagramInstanceService;
    private final InstagramCookieConvertToInstance instagramCookieConvertToInstance;

    public InstagramServiceImpl(InstagramInstanceService instagramInstanceService,
                                InstagramCookieConvertToInstance instagramCookieConvertToInstance) {
        this.instagramInstanceService = instagramInstanceService;
        this.instagramCookieConvertToInstance = instagramCookieConvertToInstance;
    }

    @Override
    public InstagramSearchUsernameResult getUsernameRequest(User user, String instagramLogin) {

        Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceByInstagramUsernameAndUser(instagramLogin, user);

        try {
            return instagram4j.sendRequest(new InstagramSearchUsernameRequest(instagramLogin));
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        throw new RuntimeException("Unfortunately we didn't find this instagram account");
    }

    /**
     *
     * @param user
     * @param instagramLogin
     * @param nextMaxId
     * @return
     */
    @Override
    public InstagramFeedResult getPostsRequest(User user, String instagramLogin, String nextMaxId) {
        try {
            Instagram4j instagram4j = instagramInstanceService.getInstagramInstanceByInstagramUsernameAndUser(instagramLogin, user);

            InstagramSearchUsernameResult result = instagram4j.sendRequest(new InstagramSearchUsernameRequest(instagramLogin));

            //The first iteration returns about 18-20 items
            return instagram4j.sendRequest(new InstagramUserFeedRequest(result.getUser().getPk(), nextMaxId, 0));
        } catch (IOException e) {
            log.error(String.format("Unfortunately we didn't find this instagram account: %s for user username: %s", instagramLogin, user.getEmail()));
            throw new RuntimeException("Unfortunately we didn't find this instagram account");
        }
    }

    //TODO: Rewise
    @Override
    public InstagramGetUserFollowersResult getFollowersRequest(UserCommand userCommand, String instagramId) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        final Long instaId = Long.valueOf(instagramId);

        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getId().equals(instaId)) {
                Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(instaId);
                try {
                    InstagramInstance instance = instagramCookieConvertToInstance.convert(cookie);

                    InstagramSearchUsernameResult result = instagram4j.sendRequest(new InstagramSearchUsernameRequest(instance.getUsername()));

                    return instagram4j.sendRequest(new InstagramGetUserFollowersRequest(result.getUser().getPk()));
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            } else {
                log.error(String.format("Unfortunately we didn't find this instagram account id: %d for user username: %s", instaId, userCommand.getLogin()));
                throw new RuntimeException("Unfortunately we didn't find this instagram account");
            }
        }
        throw new RuntimeException("Unfortunately we didn't find this instagram account");
    }

    //TODO: Rewise
    @Override
    public InstagramGetUserFollowersResult getFollowingRequest(UserCommand userCommand, String instagramId) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        final Long instaId = Long.valueOf(instagramId);

        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getId().equals(instaId)) {
                Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(instaId);
                try {
                    InstagramInstance instance = instagramCookieConvertToInstance.convert(cookie);

                    InstagramSearchUsernameResult result = instagram4j.sendRequest(new InstagramSearchUsernameRequest(instance.getUsername()));

                    return instagram4j.sendRequest(new InstagramGetUserFollowingRequest(result.getUser().getPk()));
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            } else {
                log.error(String.format("Unfortunately we didn't find this instagram account id: %d for user username: %s", instaId, userCommand.getLogin()));
                throw new RuntimeException("Unfortunately we didn't find this instagram account");
            }
        }
        throw new RuntimeException("Unfortunately we didn't find this instagram account");
    }

    @Override
    public InstagramFeedResultCommand getFeedResultByTag(Long instagramCredentialId, String tag) {
        try {
            InstagramFeedResultCommand feedResultCommand = new InstagramFeedResultCommand();

            Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(instagramCredentialId);

            int numberOfRestReq = 0;
            String maxId = "";
            while (numberOfRestReq < 100) {
                InstagramFeedResult feedResult = instagram4j.sendRequest(new InstagramTagFeedRequest(tag, maxId));

                numberOfRestReq += feedResult.getItems().size();

                maxId = feedResult.getNext_max_id();

                List<InstagramFeedItem> instagramFeedItems = feedResult.getItems();
                List<InstagramFeedItem> instagramRankedFeedItems = feedResult.getRanked_items();

                if (instagramFeedItems != null) {
                    feedResultCommand.getItems().addAll(feedResult.getItems());
                }

                if (instagramRankedFeedItems != null) {
                    feedResultCommand.getRanked_items().addAll(feedResult.getRanked_items());
                }
            }

            return feedResultCommand;
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }

        throw new RuntimeException("Something went wrong!");
    }

    @Override
    public InstagramFeedResult executeTagRequest(UserCommand userCommand, String instagramLogin, IntsagramJobCommand command) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                Instagram4j instagram4j = this.instagramInstanceService.getInstagram4j(cookie);
                try {
                    //Liking by tags
                    for (Object tag : command.getTags().stream().filter(s -> (s != null && !s.isEmpty())).toArray()) {

                        int numberOfRestReq = 0;
                        String maxId = "";
                        while (numberOfRestReq < command.getNumber()) {
                            InstagramFeedResult feedResult = instagram4j.sendRequest(new InstagramTagFeedRequest(String.valueOf(tag), maxId));

                            numberOfRestReq = executeJobs(feedResult, instagram4j, command, String.valueOf(tag), numberOfRestReq);
                            maxId = feedResult.getNext_max_id();
                        }
                    }

                    //Liking by Location
                    for (Object location : command.getLocations().stream().filter(s -> (s != null && !s.isEmpty())).toArray()) {
                        int numberOfRestReq = 0;
                        String maxId = "";
                        while (numberOfRestReq < command.getNumber()) {
                            InstagramFeedResult feedResult = instagram4j.sendRequest(new InstagramLocationFeedRequest(String.valueOf(location), maxId));

                            numberOfRestReq = executeJobs(feedResult, instagram4j, command, String.valueOf(location), numberOfRestReq);
                            maxId = feedResult.getNext_max_id();
                        }
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            } else {
                log.error(String.format("Unfortunately we didn't find this instagram account: %s for user username: %s", instagramLogin, userCommand.getLogin()));
                throw new RuntimeException("Unfortunately we didn't find this instagram account");
            }
        }
        return null;
    }

    private int executeJobs(InstagramFeedResult feedResult, Instagram4j instagram4j, IntsagramJobCommand command, String string, int numberOfRestReq) {
        int likingCount = 0;
        int followCount = 0;


        for (InstagramFeedItem feedItem : feedResult.getItems()) {
            if (numberOfRestReq < command.getNumber()) {
                likingCount++;
                numberOfRestReq++;
                likeByFeedItem(instagram4j, feedItem);

                if (command.getIsFollow()) {
                    followCount++;
                    followByInstagramUser(instagram4j, feedItem.getUser());
                }
            } else {
                break;
            }
        }

        log.debug("Liked by tag: " + string + ", times=" + likingCount);

        if (followCount > 0) {
            log.debug("Followed by tag: " + string + ", times=" + followCount);
        }

        return numberOfRestReq;
    }

    @Override
    public void likeFeedItems(Long credentialId, List<InstagramFeedItem> feedItems) {
        Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(credentialId);

        for (InstagramFeedItem feedItem : feedItems) {
            likeByFeedItem(instagram4j, feedItem);
        }
    }

    @Override
    public InstagramLikeResult likeByFeedItem(Instagram4j instagram4j, InstagramFeedItem feedItem) {
        try {
            InstagramLikeResult likeResult = instagram4j.sendRequest(new InstagramLikeRequest(feedItem.getPk()));
            log.debug("Liked by MediaID: " + feedItem.getPk() + ", https://www.instagram.com/p/" + feedItem.getCode());

            Utils.randomWaiting();

            return likeResult;
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        }
        throw new RuntimeException("Something went wrong during executing like request.");
    }

    @Override
    public void followInstagramUsers(Long credentialId, List<InstagramFeedItem> feedItems) {
        Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(credentialId);

        for (InstagramFeedItem feedItem : feedItems) {
            followByInstagramUser(instagram4j, feedItem.getUser());
        }
    }

    @Override
    public StatusResult followByInstagramUser(Instagram4j instagram4j, InstagramUser user) {
        try {
            StatusResult followResult = instagram4j.sendRequest(new InstagramFollowRequest(user.getPk()));
            log.debug("Followed status: " + followResult.getStatus() + ", message: " + followResult.getMessage());

            Utils.randomWaiting();

            return followResult;
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        }
        throw new RuntimeException("Something went wrong during executing like request.");
    }

    @Override
    public void postComments(Long credentialId, List<InstagramFeedItem> feedItems, final String commentText) {
        Instagram4j instagram4j = this.instagramInstanceService.getInstagramInstanceById(credentialId);

        for (InstagramFeedItem feedItem : feedItems) {
            Long mediaId = Long.valueOf(feedItem.getId());
            postComment(instagram4j, mediaId, commentText);
        }
    }

    @Override
    public InstagramPostCommentResult postComment(Instagram4j instagram4j, long mediaId, String commentText) {
        try {
            InstagramPostCommentResult postCommentResult = instagram4j.sendRequest(new InstagramPostCommentRequest(mediaId, commentText));
            log.debug("Followed status: " + postCommentResult.getStatus() + ", message: " + postCommentResult.getMessage());

            Utils.randomWaiting();

            return postCommentResult;
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        }
        throw new RuntimeException("Something went wrong during posting a comment request.");
    }

    /**
     * Get all followed users by id
     *
     * @param userID      - ID of interested user
     * @param instagram4j - API
     * @return - list of followers
     */
    @Override
    public List<InstagramUserSummary> getFollowedUsersByUserID(Long userID, Instagram4j instagram4j) {
        try {
            List<InstagramUserSummary> followersList = new ArrayList<>();

            String maxID = "";
            while (maxID != null) {
                InstagramGetUserFollowersResult followersResult = instagram4j.sendRequest(new InstagramGetUserFollowersRequest(userID, maxID));

                followersList.addAll(followersResult.getUsers());
                maxID = followersResult.getNext_max_id();

                Utils.randomWaiting();
            }

            return followersList;
        } catch (InterruptedException | IOException e) {
            log.error(e.getMessage());
        }

        throw new InstagramUsersNotFound();
    }

    /**
     * Get all following users by id
     *
     * @param userID      - ID of interested user
     * @param instagram4j - API
     * @return - list of following users
     */
    @Override
    public List<InstagramUserSummary> getFollowingUsersByUserID(Long userID, Instagram4j instagram4j) {
        try {
            List<InstagramUserSummary> followersList = new ArrayList<>();

            String maxID = "";
            while (maxID != null) {
                InstagramGetUserFollowersResult followersResult = instagram4j.sendRequest(new InstagramGetUserFollowingRequest(userID, maxID));

                followersList.addAll(followersResult.getUsers());
                maxID = followersResult.getNext_max_id();

                Utils.randomWaiting();
            }

            return followersList;
        } catch (InterruptedException | IOException e) {
            log.error(e.getMessage());
        }

        throw new InstagramUsersNotFound();
    }

    @Override
    public void unfollow(Instagram4j instagram4j, Long pk) {
        try {
            instagram4j.sendRequest(new InstagramUnfollowRequest(pk));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
