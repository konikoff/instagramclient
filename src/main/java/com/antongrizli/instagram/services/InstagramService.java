package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.InstagramFeedResultCommand;
import com.antongrizli.instagram.commands.IntsagramJobCommand;
import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.User;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.*;

import java.util.List;

public interface InstagramService {

    InstagramSearchUsernameResult getUsernameRequest(User user, String instagramLogin);

    InstagramFeedResult getPostsRequest(User user, String instagramLogin, String nextMaxId);

    InstagramGetUserFollowersResult getFollowersRequest(UserCommand userCommand, String instagramId);

    InstagramGetUserFollowersResult getFollowingRequest(UserCommand userCommand, String instagramId);

    InstagramFeedResultCommand getFeedResultByTag(Long instagramCredentialId, String tag);

    InstagramFeedResult executeTagRequest(UserCommand userCommand, String instagramLogin, IntsagramJobCommand command);

    void likeFeedItems(Long credentialId, List<InstagramFeedItem> feedItems);

    InstagramLikeResult likeByFeedItem(Instagram4j instagram4j, InstagramFeedItem feedItem);

    void followInstagramUsers(Long credentialId, List<InstagramFeedItem> feedItems);

    StatusResult followByInstagramUser(Instagram4j instagram4j, InstagramUser user);

    void postComments(Long credentialId, List<InstagramFeedItem> feedItems, String commentText);

    InstagramPostCommentResult postComment(Instagram4j instagram4j, long mediaId, String commentText);

    List<InstagramUserSummary> getFollowedUsersByUserID(Long userID, Instagram4j instagram4j);

    List<InstagramUserSummary> getFollowingUsersByUserID(Long userID, Instagram4j instagram4j);

    void unfollow(Instagram4j instagram4j, Long pk);
}
