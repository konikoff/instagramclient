package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.commands.InstagramCredentialsCommand;

public interface InstagramAuthService {

    void login(InstagramCredentialsCommand credentials, UserCommand userCommand);

    void logout();
}
