package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.RegistrationFormCommand;
import com.antongrizli.instagram.domain.User;

import java.util.List;

public interface UserService {

    @Deprecated
    void save(User user);

    User registerNewUserAccount(RegistrationFormCommand form);

    List<User> findAll();

    User findByLogin(String login);

    void delete(User user);

    void deleteAll();
}
