package com.antongrizli.instagram.services;


import com.antongrizli.instagram.configurations.security.CustomUserPrincipalImpl;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("customUserDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = this.userRepository.findByEmail(username).orElse(null);
        if (user == null) {
            log.error("User not found: " + username);
            throw new UsernameNotFoundException("User not found: " + username);
        }

        return new CustomUserPrincipalImpl(user);
    }
}

