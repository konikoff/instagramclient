package com.antongrizli.instagram.services;

import com.antongrizli.instagram.domain.InstagramCredentials;

import java.util.List;

public interface InstagramCredentialService {
    List<InstagramCredentials> getAllCredentials();

    void save(InstagramCredentials credentials);
}
