package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.InstagramUserSummaryToInstagramFollowingUser;
import com.antongrizli.instagram.converters.InstagramUserSummaryToInstagramUser;
import com.antongrizli.instagram.domain.*;
import com.antongrizli.instagram.repositories.InstagramFollowedUserRepository;
import com.antongrizli.instagram.repositories.InstagramFollowingUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Slf4j
@Service
public class InstagramFollowerUserServiceImpl implements InstagramFollowerUserService {

    private final InstagramFollowedUserRepository instagramFollowedUserRepository;
    private final InstagramFollowingUserRepository instagramFollowingUserRepository;
    private final InstagramInstanceService instagramInstanceService;
    private final InstagramService instagramService;
    private final InstagramUserSummaryToInstagramUser userSummaryToInstagramUser;
    private final InstagramUserSummaryToInstagramFollowingUser instagramUserSummaryToInstagramFollowingUser;

    public InstagramFollowerUserServiceImpl(InstagramFollowedUserRepository instagramFollowedUserRepository,
                                            InstagramFollowingUserRepository instagramFollowingUserRepository,
                                            InstagramInstanceService instanceInstanceService,
                                            InstagramService instagramService,
                                            InstagramUserSummaryToInstagramUser userSummaryToInstagramUser,
                                            InstagramUserSummaryToInstagramFollowingUser instagramUserSummaryToInstagramFollowingUser) {
        this.instagramFollowedUserRepository = instagramFollowedUserRepository;
        this.instagramFollowingUserRepository = instagramFollowingUserRepository;
        this.instagramInstanceService = instanceInstanceService;
        this.instagramService = instagramService;
        this.userSummaryToInstagramUser = userSummaryToInstagramUser;
        this.instagramUserSummaryToInstagramFollowingUser = instagramUserSummaryToInstagramFollowingUser;
    }

    @Override
    @Deprecated
    @Transactional
    public void saveFollowers(Long credentialId, List<? extends InstagramFollowerUser> instagramFollowedUsers) {
        List<InstagramFollowedUser> savedFollowedUserList = instagramFollowedUserRepository.findAllByInstagramCredentials_Id(credentialId).orElse(new ArrayList<>());

        checkUsers(savedFollowedUserList, instagramFollowedUsers);
    }

    @Override
    public void syncFollowedUsers(String instagramLogin, UserCommand userCommand) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                Instagram4j instagram4j = instagramInstanceService.getInstagram4j(cookie);

                List<InstagramUserSummary> followersList = instagramService.getFollowedUsersByUserID(cookie.getInstagramUserId(), instagram4j);

                List<InstagramFollowedUser> instagramFollowedUsers = new ArrayList<>();
                followersList.forEach(instagramUserSummary -> {
                    InstagramFollowedUser instagramFollowedUser = userSummaryToInstagramUser.convert(instagramUserSummary);
                    instagramFollowedUser.setInstagramCredentials(cookie);
                    instagramFollowedUsers.add(instagramFollowedUser);
                });

                saveFollowedUsers(instagramFollowedUsers);

                log.debug("Fetched: " + followersList.size() + " followers");
            }
        }

    }

    @Override
    public void syncFollowingUsers(String instagramLogin, UserCommand userCommand) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                Instagram4j instagram4j = instagramInstanceService.getInstagram4j(cookie);

                List<InstagramUserSummary> followersList = instagramService.getFollowingUsersByUserID(cookie.getInstagramUserId(), instagram4j);

                List<InstagramFollowingUser> instagramFollowingUsers = new ArrayList<>();
                followersList.forEach(instagramUserSummary -> {
                    InstagramFollowingUser instagramFollowingUser = instagramUserSummaryToInstagramFollowingUser.convert(instagramUserSummary);
                    instagramFollowingUser.setInstagramCredentials(cookie);
                    instagramFollowingUsers.add(instagramFollowingUser);
                });

                saveFollowingUsers(instagramFollowingUsers);

                log.debug("Fetched: " + followersList.size() + " followers");
            }
        }
    }

    @Override
    public List<InstagramFollowedUser> getFollowedUsers(String instagramLogin, UserCommand userCommand) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                return instagramFollowedUserRepository.findAllByInstagramCredentials_Id(cookie.getId()).orElse(new ArrayList<>());
            }
        }
        throw new RuntimeException("We didn't find that user " + instagramLogin);
    }

    @Override
    public List<? extends InstagramFollowerUser> getFollowingUsers(String instagramLogin, UserCommand userCommand) {
        List<InstagramCredentials> credentialsList = userCommand.getInstagramCredentials();
        for (InstagramCredentials cookie : credentialsList) {
            if (cookie.getUsername().equalsIgnoreCase(instagramLogin)) {
                return instagramFollowingUserRepository.findAllByInstagramCredentials_Id(cookie.getId()).orElse(new ArrayList<>());
            }
        }
        throw new RuntimeException("We didn't find that user " + instagramLogin);
    }


    private void checkUsers(List<? extends InstagramFollowerUser> savedFollowedUserList, List<? extends InstagramFollowerUser> newFollowedUserList) {
        // 1. Find new followers
        Set<? extends InstagramFollowerUser> savedFollowedUsers = new HashSet<>(savedFollowedUserList);
        for (InstagramFollowerUser newFollowedUser : newFollowedUserList) {
            if (!savedFollowedUsers.contains(newFollowedUser)) {
                //it's a new follower user
                instagramFollowedUserRepository.save((InstagramFollowedUser) newFollowedUser);
                log.debug("Saved follower with ID: " + newFollowedUser.getPk() + ", username: " + newFollowedUser.getUsername());
            }
        }

        // 2. Delete unfollowed users
        Set<? extends InstagramFollowerUser> newFollowedUsers = new HashSet<>(newFollowedUserList);
        for (InstagramFollowerUser savedFollowedUser : savedFollowedUserList) {
            if (!newFollowedUsers.contains(savedFollowedUser)) {
                // this user unfollowed
                instagramFollowedUserRepository.delete((InstagramFollowedUser) savedFollowedUser);
                log.debug("Unfollowed user ID: " + savedFollowedUser.getPk() + ", username: " + savedFollowedUser.getUsername());
            }
        }
    }

    @Override
    @Transactional
    public void saveFollowedUsers(List<? extends InstagramFollowerUser> followedUsers) {
        List<InstagramFollowedUser> instagramFollowedUsers = (List<InstagramFollowedUser>) followedUsers;
        for (InstagramFollowedUser followedUser : instagramFollowedUsers) {
            instagramFollowedUserRepository.insertFollowedUser(followedUser.getFullName(),
                    followedUser.getPk(),
                    followedUser.getProfilePicUrl(),
                    followedUser.getSyncDate(),
                    followedUser.getUsername(),
                    followedUser.getInstagramCredentials().getId());
        }
    }

    @Override
    public void saveFollowingUsers(List<? extends InstagramFollowerUser> followerList) {

        List<InstagramFollowingUser> instagramFollowingUsers = (List<InstagramFollowingUser>) followerList;
        for (InstagramFollowingUser followingUser : instagramFollowingUsers) {
            instagramFollowingUserRepository.insertFollowingUser(followingUser.getFullName(),
                    followingUser.getPk(),
                    followingUser.getProfilePicUrl(),
                    followingUser.getSyncDate(),
                    followingUser.getUsername(),
                    followingUser.getInstagramCredentials().getId());
        }
    }

    @Override
    @Transactional
    public List<? extends InstagramFollowerUser> getMutualUsers(String instagramLogin, User user) {
        Long instagramCredentialID = instagramInstanceService.getInstagramInstanceIdByInstagramUsernameAndUser(instagramLogin, user);

        return this.instagramFollowingUserRepository.findAllMutualUsers(instagramCredentialID);
    }

    @Override
    @Transactional
    public List<InstagramFollowingUser> getUnmutualUsers(String instagramLogin, User user) {
        Long instagramCredentialID = instagramInstanceService.getInstagramInstanceIdByInstagramUsernameAndUser(instagramLogin, user);

        return this.instagramFollowingUserRepository.findAllUnmutualUsers(instagramCredentialID);
    }

    @Override
    public void unfollowInstagramUser(String instagramLogin, User user, Long pk) {
        instagramService.unfollow(instagramInstanceService.getInstagramInstanceByInstagramUsernameAndUser(instagramLogin, user), pk);

        Long instagramCredentialId = instagramInstanceService.getInstagramInstanceIdByInstagramUsernameAndUser(instagramLogin, user);

        instagramFollowingUserRepository.deleteInstagramFollowingUserByPkAndCAndInstagramCredentials(pk, instagramCredentialId);

    }
}
