package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.commands.InstagramCredentialsCommand;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramLoginResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

@Slf4j
@Service
public class InstagramAuthServiceImpl implements InstagramAuthService {

    private final InstagramInstanceService instanceService;

    public InstagramAuthServiceImpl(InstagramInstanceService instanceService) {
        this.instanceService = instanceService;
    }

    @Override
    @Transactional
    public void login(InstagramCredentialsCommand credentials, UserCommand userCommand) {
        try {
            Instagram4j instagram = Instagram4j.builder().username(credentials.getLogin()).password(credentials.getPassword()).build();
            instagram.setup();

            InstagramLoginResult loginResult = instagram.login();

            log.debug("Login result: " + loginResult.getMessage());
            switch (loginResult.getStatus().toLowerCase()) {
                case "ok":
                    instanceService.save(instagram, userCommand);
                    break;
                default:
                    log.debug("Please username again");
                    break;
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

    }

    @Override
    @Transactional
    public void logout() {

    }
}
