package com.antongrizli.instagram.schedule;

import com.antongrizli.instagram.converters.InstagramUserSummaryToInstagramUser;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramFollowedUser;
import com.antongrizli.instagram.services.InstagramCredentialService;
import com.antongrizli.instagram.services.InstagramFollowerUserService;
import com.antongrizli.instagram.services.InstagramInstanceService;
import com.antongrizli.instagram.services.InstagramService;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class SyncFollowers {
    //https://www.freeformatter.com/cron-expression-generator-quartz.html
    private static final String syncTimePattern = "* 0 */24 * * *"; //execute each 0 minute after 24 hours: * 0 */24 * * *

    private final InstagramCredentialService credentialService;
    private final InstagramInstanceService instanceInstanceService;
    private final InstagramUserSummaryToInstagramUser userSummaryToInstagramUser;
    private final InstagramService instagramService;
    private final InstagramFollowerUserService instagramFollowerUserService;

    public SyncFollowers(InstagramCredentialService credentialService,
                         InstagramInstanceService instanceInstanceService,
                         InstagramUserSummaryToInstagramUser userSummaryToInstagramUser,
                         InstagramService instagramService,
                         InstagramFollowerUserService instagramFollowerUserService) {
        this.credentialService = credentialService;
        this.instanceInstanceService = instanceInstanceService;
        this.userSummaryToInstagramUser = userSummaryToInstagramUser;
        this.instagramService = instagramService;
        this.instagramFollowerUserService = instagramFollowerUserService;
    }

    //Useful link: https://crontab.guru
    @Scheduled(cron = syncTimePattern)
    public void syncFollowers() {

        List<InstagramCredentials> instanceCredentialsList = credentialService.getAllCredentials();
        for (InstagramCredentials credentials : instanceCredentialsList) {
            try {
                new FollowersJob(credentials);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        }

        log.debug("Task is finished");
    }

    private class FollowersJob implements Runnable {
        private static final String THREAD_NAME = "Followers-Thread";
        private final InstagramCredentials instagramCredentials;

        private Semaphore semaphore = new Semaphore(0);

        private FollowersJob(InstagramCredentials instagramCredentials) throws InterruptedException {
            this.instagramCredentials = instagramCredentials;

            Thread followersThread = new Thread(this);
            followersThread.setName(THREAD_NAME);

            followersThread.start();

            TimeUnit.SECONDS.sleep(5);

            semaphore.release(1);
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                Instagram4j instagram4j = instanceInstanceService.getInstagram4j(instagramCredentials);

                List<InstagramUserSummary> followersList = instagramService.getFollowedUsersByUserID(instagramCredentials.getInstagramUserId(), instagram4j);

                List<InstagramFollowedUser> instagramFollowedUsers = new ArrayList<>();
                followersList.forEach(instagramUserSummary -> {
                    InstagramFollowedUser instagramFollowedUser = userSummaryToInstagramUser.convert(instagramUserSummary);
                    instagramFollowedUser.setInstagramCredentials(instagramCredentials);
                    instagramFollowedUsers.add(instagramFollowedUser);
                });

                instagramFollowerUserService.saveFollowers(instagramCredentials.getId(), instagramFollowedUsers);

                log.debug("Thread - " + THREAD_NAME + ": finished");
                log.debug("Fetched: " + followersList.size() + " followers");

                semaphore.release();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        }
    }

    private class FollowingsJob implements Runnable {
        private static final String THREAD_NAME = "Followings-Thread";
        private final InstagramCredentials instagramCredentials;

        Semaphore semaphore = new Semaphore(0);

        private FollowingsJob(InstagramCredentials instagramCredentials) {
            this.instagramCredentials = instagramCredentials;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                log.debug("Thread - " + THREAD_NAME + ": finished");
                semaphore.release();
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        }
    }

}
