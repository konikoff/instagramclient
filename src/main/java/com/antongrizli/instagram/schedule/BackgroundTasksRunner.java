package com.antongrizli.instagram.schedule;

import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import com.antongrizli.instagram.schedule.concurrent.TaskThread;
import com.antongrizli.instagram.schedule.concurrent.TaskThreadKey;
import com.antongrizli.instagram.services.InstagramService;
import com.antongrizli.instagram.services.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@Slf4j
@Component
public class BackgroundTasksRunner {

    //https://www.freeformatter.com/cron-expression-generator-quartz.html
    private static final String syncTimePattern = "0 0/5 * ? * *";

    private final TaskService taskService;
    private final InstagramService instagramService;

    private Map<Long, List<Task>> taskMap = new HashMap<>();

    private static final int TASK_CAPACITY = 10;
    private BlockingQueue<TaskThreadKey> tasksQueue = new ArrayBlockingQueue<>(TASK_CAPACITY);

    private TaskThread taskThread;


    public BackgroundTasksRunner(TaskService taskService,
                                 InstagramService instagramService) {
        this.taskService = taskService;
        this.instagramService = instagramService;

        this.taskThread = new TaskThread(tasksQueue, instagramService, taskService);
        taskThread.start();
    }

    @Scheduled(cron = syncTimePattern)
    public void checkSavedTasks() {
        List<Task> taskList = taskService.getWaitingTasks();

        prepareTaskMap(taskList);

        execute();
    }

    private Map<Long, List<Task>> prepareTaskMap(List<Task> taskList) {

        for (Task task : taskList) {
            Long credentialsId = task.getInstagramCredentials().getId();
            if (taskMap.containsKey(credentialsId)) {
                taskMap.get(credentialsId).add(task);
            } else {
                taskMap.put(credentialsId, new ArrayList<Task>() {{
                    add(task);
                }});
            }
        }

        return taskMap;
    }

    private void execute() {
        boolean isQueueFull = false;

        for (Map.Entry<Long, List<Task>> taskEntry : taskMap.entrySet()) {
            final Long credentialId = taskEntry.getKey();

            for (Task task : taskEntry.getValue()) {

                if (!isQueueFull) {

                    Instant lastRun = task.getLastRun();
                    boolean isTaskCreated45MinAgo = false;

                    if (lastRun != null) {

                        Instant lastRunPlus45Min = lastRun.plus(45, ChronoUnit.MINUTES);
                        Instant currentTime = Instant.now();

                        isTaskCreated45MinAgo = currentTime.isAfter(lastRunPlus45Min);
                    }


                    if (isTaskCreated45MinAgo || lastRun == null) {
                        task.setStatus(TaskStatus.RUNNING);
                        taskService.save(task);

                        isQueueFull = tasksQueue.offer(new TaskThreadKey(credentialId, task));
                    }
                } else {
                    log.debug("The task queue is full. Capacity: " + tasksQueue.size() + ", skipping Task ID: " + task.getId() + ", Task Name: " + task.getTaskName());
                    isQueueFull = tasksQueue.remainingCapacity() < 10; //FIXME: Should be synced to prevent false result as the queue could be used

                }
            }

            taskMap.remove(credentialId);
        }
    }
}
