package com.antongrizli.instagram.schedule.concurrent;

import com.antongrizli.instagram.domain.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskThreadKey {
    private Long credentialId;
    private Task task;
}
