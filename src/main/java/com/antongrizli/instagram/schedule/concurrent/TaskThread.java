package com.antongrizli.instagram.schedule.concurrent;

import com.antongrizli.instagram.commands.InstagramFeedResultCommand;
import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskAction;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import com.antongrizli.instagram.services.InstagramService;
import com.antongrizli.instagram.services.TaskService;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class TaskThread extends Thread {

    private final InstagramService instagramService;
    private final TaskService taskService;

    private BlockingQueue<TaskThreadKey> tasksQueue;

    public TaskThread(BlockingQueue<TaskThreadKey> tasksQueue,
                      InstagramService instagramService,
                      TaskService taskService) {
        this.tasksQueue = tasksQueue;
        this.instagramService = instagramService;
        this.taskService = taskService;

        setName("TaskThread");
    }

    //TODO: Implement for ranked feed items
    @Override
    public void run() {
        do {
            TaskThreadKey taskThreadKey = null;
            try {
                taskThreadKey = tasksQueue.take();

                Task task = taskThreadKey.getTask();
                Long credentialID = taskThreadKey.getCredentialId();

                InstagramFeedResultCommand feedResult = instagramService.getFeedResultByTag(credentialID, task.getTag());
                TaskAction taskAction = task.getAction();
                switch (taskAction) {

                    case LIKE:
                        instagramService.likeFeedItems(credentialID, feedResult.getItems());
                        break;
                    case FOLLOW:
                        instagramService.followInstagramUsers(credentialID, feedResult.getItems());
                        break;
                    case LEAVE_COMMENT:
                        instagramService.postComments(credentialID, feedResult.getItems(), task.getCommentText());
                        break;
                    case ALL:
                        //TODO: implement a method to execute above requests
                        break;
                    default:
                        log.debug("Thread name: " + Thread.currentThread().getName() + ", undefined action: " + taskAction.getText());
                        break;
                }

            } catch (InterruptedException e) {
                log.error(e.getMessage());
            } finally {
                if (taskThreadKey != null)
                    finishedTask(taskThreadKey.getTask());
            }

            try {
                Thread.sleep(5000);
                log.debug("TaskThread sleeping for 5 sec");
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }

        } while (true);
    }

    private void finishedTask(Task task) {
        task.setStatus(TaskStatus.WAITING);
        task.setLastRun(Instant.now());
        taskService.save(task);
    }
}
