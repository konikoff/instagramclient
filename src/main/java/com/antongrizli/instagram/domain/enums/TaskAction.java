package com.antongrizli.instagram.domain.enums;

public enum TaskAction {
    FOLLOW("Follow"),
    LIKE("Like"),
    LEAVE_COMMENT("Leave comment"),
    ALL("All");

    private final String text;

    TaskAction(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
