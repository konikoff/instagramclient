package com.antongrizli.instagram.domain.enums;

public enum TaskStatus {
    NEW, DONE, WAITING, RUNNING
}
