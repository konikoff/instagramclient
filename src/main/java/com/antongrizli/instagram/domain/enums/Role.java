package com.antongrizli.instagram.domain.enums;

public enum Role {
    USER("USER_ROLE"), ADMIN("ADMIN_ROLE");

    private final String text;

    Role(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
