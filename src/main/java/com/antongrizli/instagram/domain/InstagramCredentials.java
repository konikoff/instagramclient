package com.antongrizli.instagram.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "instagram_cookie")
public class InstagramCredentials extends BaseEntity {

    private static final long serialVersionUID = -6688935335049282914L;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "instagramUserId")
    private Long instagramUserId;

    @Column(name = "uuid")
    private String uuid;

    @Lob
    @Column(name = "cookie_store")
    private Byte[] bytesCookieStore;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instagramCredentials")
    private List<InstagramFollowedUser> followers = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instagramCredentials")
    private List<InstagramFollowingUser> followings = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instagramCredentials")
    private List<Task> tasks = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
