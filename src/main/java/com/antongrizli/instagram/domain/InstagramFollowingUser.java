package com.antongrizli.instagram.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "instagram_following_users")
public class InstagramFollowingUser extends InstagramFollowerUser {

    private static final long serialVersionUID = 4488290363077159046L;
}
