package com.antongrizli.instagram.domain;

import com.antongrizli.instagram.domain.enums.TaskAction;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Setter
@Getter
@Entity
@Table(name = "tasks")
public class Task extends BaseEntity {

    private static final long serialVersionUID = 4563347180021031771L;

    @Column(name = "task_name")
    private String taskName;

    @Lob
    @Column(name = "frequency")
    private Frequency frequency;

    @Enumerated
    @Column(name = "status")
    private TaskStatus status;

    @Column(name = "comment")
    private String commentText;

    @Column(name = "tag")
    private String tag;

    @Column(name = "last_run")
    private Instant lastRun;

    @Enumerated
    @Column(name = "action")
    private TaskAction action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "instagram_cookie_id")
    private InstagramCredentials instagramCredentials;
}
