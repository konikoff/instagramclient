package com.antongrizli.instagram.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "instagram_followed_users")
public class InstagramFollowedUser extends InstagramFollowerUser {

    private static final long serialVersionUID = 683461745906481115L;
}
