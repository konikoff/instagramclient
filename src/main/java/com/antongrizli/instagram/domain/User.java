package com.antongrizli.instagram.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 8518885914676456332L;

    @Column(name = "fullName")
    private String fullName;

    @Column(name = "email", unique = true)
    private String email;

    @Lob
    @Column(name = "password")
    private String password;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Authorities> roles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<InstagramCredentials> instagramCredentials = new ArrayList<>();

    public void assignRole(Authorities authorities) {
        roles.add(authorities);
        authorities.setUser(this);
    }

    public void removeRole(Authorities authorities) {
        roles.remove(authorities);
        authorities.setUser(null);
    }

    public void addInstaCookie(InstagramCredentials instagramCredentials) {
        this.instagramCredentials.add(instagramCredentials);
        instagramCredentials.setUser(this);
    }

    public void removeInstaCookie(InstagramCredentials instagramCredentials) {
        this.instagramCredentials.remove(instagramCredentials);
        instagramCredentials.setUser(null);
    }

    @Override
    public String toString() {
        return "User{" +
                "fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
