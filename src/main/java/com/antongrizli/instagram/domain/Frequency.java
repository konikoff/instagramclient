package com.antongrizli.instagram.domain;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Frequency implements Serializable {


    private static final long serialVersionUID = 732102476078952437L;

    private Periodicity periodicity;
    private int value;

    public Periodicity getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(Periodicity periodicity) {
        this.periodicity = periodicity;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public enum Periodicity {
        ONCE("Once"),
        MINUTE("Minute"),
        HOUR("Hour"),
        DAY("Day"),
        MONTH("Month");

        private final String text;

        Periodicity(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }
}
