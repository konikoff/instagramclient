package com.antongrizli.instagram.domain;

import com.antongrizli.instagram.domain.enums.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "authorities")
public class Authorities extends BaseEntity {

    private static final long serialVersionUID = -457974125421202395L;

    @Enumerated
    @Column(name = "role")
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
