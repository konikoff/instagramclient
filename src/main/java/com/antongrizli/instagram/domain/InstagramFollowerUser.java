package com.antongrizli.instagram.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class InstagramFollowerUser extends InstagramUser {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "instagram_cookie_id")
    private InstagramCredentials instagramCredentials;
}
