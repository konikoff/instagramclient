package com.antongrizli.instagram.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.Instant;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class InstagramUser extends BaseEntity {

    private static final long serialVersionUID = 2264605982399551251L;

    @Column(name = "pk", unique = true)
    private Long pk;

    @Column(name = "username")
    private String username;

    @Column(name = "profile_pic_url")
    private String profilePicUrl;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "sync_date")
    private Instant syncDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InstagramFollowedUser)) return false;
        InstagramFollowedUser that = (InstagramFollowedUser) o;
        return Objects.equals(getPk(), that.getPk()) &&
                Objects.equals(getUsername(), that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPk(), getUsername());
    }
}
