package com.antongrizli.instagram.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class InstagramInstance {

    private Long id;
    private String username;
    private String expiry;
}
