package com.antongrizli.instagram.commands;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InstagramAccountCommand {
    private String username;
    private String accountUrl;
    private String imageUrl;
    private String fullName;
    private int followerCount;
    private int followingCount;
    private int posts;
}
