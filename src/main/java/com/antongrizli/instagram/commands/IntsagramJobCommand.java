package com.antongrizli.instagram.commands;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class IntsagramJobCommand {
    private int number = 10;
    private String pageTitle;
    private Boolean isFollow = false;
    private List<String> tags = new ArrayList<>();
    private List<String> locations = new ArrayList<>();

}
