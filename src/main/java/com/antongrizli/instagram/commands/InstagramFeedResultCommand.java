package com.antongrizli.instagram.commands;

import lombok.Getter;
import lombok.Setter;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedItem;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class InstagramFeedResultCommand {
    private List<InstagramFeedItem> items = new ArrayList<>();
    private List<InstagramFeedItem> ranked_items = new ArrayList<>();
}
