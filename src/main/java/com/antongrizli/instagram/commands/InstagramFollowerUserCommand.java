package com.antongrizli.instagram.commands;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InstagramFollowerUserCommand {
    private String pictureUrl;
    private String username;
    private Boolean isFavorite;
    private Long instagramPk;
}
