package com.antongrizli.instagram.commands;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InstagramPostCommand {
    private String description;
    private int commentCount;
    private int likesCount;
    private String picUrl;
    private long mediaId;
}
