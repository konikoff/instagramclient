package com.antongrizli.instagram.commands;

import com.antongrizli.instagram.domain.Frequency;
import com.antongrizli.instagram.domain.enums.TaskAction;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskCommand {

    private Long id;

    private String taskName;

    private Frequency.Periodicity periodicity = Frequency.Periodicity.ONCE;

    private int periodicityValue = 5;

    private TaskStatus status = TaskStatus.NEW;

    private String comment;

    private String tag;

    private TaskAction action = TaskAction.LIKE;
}

