package com.antongrizli.instagram.commands;

import com.antongrizli.instagram.domain.BaseEntity;
import com.antongrizli.instagram.domain.InstagramCredentials;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserCommand extends BaseEntity {

    private static final long serialVersionUID = -5391437560696430838L;

    @NotNull
    @NotEmpty
    private String login;

    @NotNull
    @NotEmpty
    private String password;

    private boolean isChecked;

    private List<InstagramCredentials> instagramCredentials = new ArrayList<>();
}
