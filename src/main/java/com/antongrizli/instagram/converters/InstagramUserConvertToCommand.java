package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.Constants;
import com.antongrizli.instagram.commands.InstagramAccountCommand;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class InstagramUserConvertToCommand implements Converter<InstagramUser, InstagramAccountCommand> {

    @Override
    public InstagramAccountCommand convert(InstagramUser instagramUser) {
        InstagramAccountCommand accountCommand = new InstagramAccountCommand();
        accountCommand.setFollowerCount(instagramUser.getFollower_count());
        accountCommand.setFollowingCount(instagramUser.getFollowing_count());
        accountCommand.setFullName(instagramUser.getFull_name());
        accountCommand.setImageUrl(instagramUser.getProfile_pic_url());
        accountCommand.setPosts(instagramUser.getMedia_count());
        accountCommand.setUsername(instagramUser.getUsername());
        accountCommand.setAccountUrl(Constants.INSTAGRAM_URL + instagramUser.getUsername());

        return accountCommand;
    }
}
