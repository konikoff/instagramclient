package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.domain.InstagramFollowingUser;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Slf4j
@Component
public class InstagramUserSummaryToInstagramFollowingUser implements Converter<InstagramUserSummary, InstagramFollowingUser> {

    @Override
    public InstagramFollowingUser convert(InstagramUserSummary source) {
        InstagramFollowingUser instagramFollowingUser = new InstagramFollowingUser();

        instagramFollowingUser.setPk(source.getPk());
        instagramFollowingUser.setFullName(source.getFull_name());
        instagramFollowingUser.setProfilePicUrl(source.getProfile_pic_url());
        instagramFollowingUser.setUsername(source.getUsername());
        instagramFollowingUser.setSyncDate(Instant.now());

        return instagramFollowingUser;
    }
}
