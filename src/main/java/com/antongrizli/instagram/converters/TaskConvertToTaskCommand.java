package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.TaskCommand;
import com.antongrizli.instagram.domain.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TaskConvertToTaskCommand implements Converter<Task, TaskCommand> {
    @Override
    public TaskCommand convert(Task source) {
        TaskCommand command = new TaskCommand();

        if (source.getId() != null) {
            command.setId(source.getId());
        }

        command.setId(source.getId());
        command.setTaskName(source.getTaskName());
        command.setPeriodicity(source.getFrequency().getPeriodicity());
        command.setPeriodicityValue(source.getFrequency().getValue());
        command.setStatus(source.getStatus());
        command.setComment(source.getCommentText());
        command.setTag(source.getTag());
        command.setAction(source.getAction());

        return command;
    }
}
