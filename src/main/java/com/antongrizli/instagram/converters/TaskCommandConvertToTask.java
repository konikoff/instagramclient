package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.TaskCommand;
import com.antongrizli.instagram.domain.Frequency;
import com.antongrizli.instagram.domain.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TaskCommandConvertToTask implements Converter<TaskCommand, Task> {
    @Override
    public Task convert(TaskCommand taskCommand) {
        Task task = new Task();

        if (taskCommand.getId() != null) {
            task.setId(taskCommand.getId());
        }

        task.setTaskName(taskCommand.getTaskName());

        Frequency frequency = new Frequency();
        frequency.setPeriodicity(taskCommand.getPeriodicity());
        frequency.setValue(taskCommand.getPeriodicityValue());
        task.setFrequency(frequency);

        task.setStatus(taskCommand.getStatus());

        task.setCommentText(taskCommand.getComment());

        task.setTag(taskCommand.getTag());

        task.setAction(taskCommand.getAction());

        return task;
    }
}
