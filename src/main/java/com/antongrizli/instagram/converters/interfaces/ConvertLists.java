package com.antongrizli.instagram.converters.interfaces;

import org.springframework.lang.Nullable;

import java.util.List;

@FunctionalInterface
public interface ConvertLists<S, T> {

    @Nullable
    List<T> convertLists(List<S> source);
}
