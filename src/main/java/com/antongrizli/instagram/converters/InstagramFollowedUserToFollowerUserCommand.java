package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.InstagramFollowerUserCommand;
import com.antongrizli.instagram.converters.interfaces.ConvertLists;
import com.antongrizli.instagram.domain.InstagramFollowerUser;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InstagramFollowedUserToFollowerUserCommand implements Converter<InstagramFollowerUser, InstagramFollowerUserCommand>, ConvertLists<InstagramFollowerUser, InstagramFollowerUserCommand> {

    @Override
    public InstagramFollowerUserCommand convert(InstagramFollowerUser followedUser) {
        InstagramFollowerUserCommand followedUserCommand = new InstagramFollowerUserCommand();

        followedUserCommand.setUsername(followedUser.getUsername());
        followedUserCommand.setPictureUrl(followedUser.getProfilePicUrl());
        followedUserCommand.setInstagramPk(followedUser.getPk());

        return followedUserCommand;
    }


    @Override
    public List<InstagramFollowerUserCommand> convertLists(List<InstagramFollowerUser> followedUserList) {
        List<InstagramFollowerUserCommand> followedUserCommandList = new ArrayList<>();
        for (InstagramFollowerUser followedUser : followedUserList) {
            followedUserCommandList.add(convert(followedUser));
        }
        return followedUserCommandList;
    }
}
