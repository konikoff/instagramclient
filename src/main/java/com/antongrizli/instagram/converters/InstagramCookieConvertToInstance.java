package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.InstagramInstance;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

@Slf4j
@Component
public class InstagramCookieConvertToInstance implements Converter<InstagramCredentials, InstagramInstance> {


    @Override
    public InstagramInstance convert(InstagramCredentials source) {
        ObjectInputStream ios = null;
        try {
            ios = new ObjectInputStream(new ByteArrayInputStream(ArrayUtils.toPrimitive(source.getBytesCookieStore())));

            CookieStore cookieStore = (CookieStore) ios.readObject();
            List<Cookie> cookies = cookieStore.getCookies();
            Cookie dsUser = cookies.stream().filter(cookie -> cookie.getName().equalsIgnoreCase("ds_user")).findFirst().get();

            return InstagramInstance.builder()
                    .id(source.getId())
                    .username(dsUser.getValue())
                    .expiry(DateFormatUtils.format(dsUser.getExpiryDate(), "yyyy-MM-dd HH:mm:SS"))
                    .build();
        } catch (IOException | ClassNotFoundException e) {
            log.error(e.getMessage());

            throw new RuntimeException(e.getMessage());
        }
    }
}
