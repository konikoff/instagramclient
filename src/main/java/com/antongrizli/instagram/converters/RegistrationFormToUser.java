package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.RegistrationFormCommand;
import com.antongrizli.instagram.domain.Authorities;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.domain.enums.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class RegistrationFormToUser implements Converter<RegistrationFormCommand, User> {

    private final PasswordEncoder passwordEncoder;

    public RegistrationFormToUser(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User convert(RegistrationFormCommand formCommand) {
        User user = new User();
        user.setFullName(formCommand.getFullName());
        user.setEmail(formCommand.getEmail());
        user.setPassword(passwordEncoder.encode(formCommand.getPassword()));

        Authorities authorities = new Authorities();
        authorities.setRole(Role.USER);

        user.assignRole(authorities);

        return user;
    }
}
