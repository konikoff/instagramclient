package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserCommandConvertToUser implements Converter<UserCommand, User> {

   /* private final PasswordEncoder passwordEncoder;

    public UserCommandConvertToUser(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
*/
    @Override
    public User convert(UserCommand userCommand) {

        User user = new User();
        user.setId(userCommand.getId());
        user.setEmail(userCommand.getLogin());
        user.setPassword(userCommand.getPassword());

        return user;
    }
}
