package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.InstagramPostCommand;
import org.brunocvcunha.instagram4j.requests.payload.ImageMeta;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedItem;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InstagramFeedResultToPostCommand implements Converter<InstagramFeedResult, List<InstagramPostCommand>> {

    @Override
    public List<InstagramPostCommand> convert(InstagramFeedResult source) {
        List<InstagramPostCommand> postCommands = new ArrayList<>();
        for (InstagramFeedItem feedItem : source.getItems()) {
            InstagramPostCommand postCommand = new InstagramPostCommand();
            postCommand.setCommentCount(feedItem.getComment_count());
            postCommand.setLikesCount(feedItem.getLike_count());


            List<ImageMeta> candidatesList;

            //check if it's carousel media type
            if (feedItem.getCarousel_media() != null) {
                candidatesList = feedItem.getCarousel_media().get(0).image_versions2.getCandidates();
            } else {
                candidatesList = feedItem.getImage_versions2().getCandidates();
            }
            ImageMeta imageMeta = candidatesList.get(0);
            postCommand.setPicUrl(imageMeta.getUrl());


            if (feedItem.getCaption() != null) {
                String description = feedItem.getCaption().getText();
                postCommand.setDescription(description);

                long mediaId = feedItem.getCaption().getMedia_id();
                postCommand.setMediaId(mediaId);
            }
            postCommands.add(postCommand);
        }

        return postCommands;
    }
}
