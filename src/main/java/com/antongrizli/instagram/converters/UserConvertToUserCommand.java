package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Slf4j
@Component
public class UserConvertToUserCommand implements Converter<User, UserCommand> {

    @Override
    public UserCommand convert(User user) {
        UserCommand command = new UserCommand();
        command.setId(user.getId());
        command.setLogin(user.getEmail());
        command.setPassword(user.getPassword());

        command.setInstagramCredentials(new ArrayList<>(user.getInstagramCredentials()));

        return command;
    }
}