package com.antongrizli.instagram.converters;

import com.antongrizli.instagram.domain.InstagramFollowedUser;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Slf4j
@Component
public class InstagramUserSummaryToInstagramUser implements Converter<InstagramUserSummary, InstagramFollowedUser> {

    @Override
    public InstagramFollowedUser convert(InstagramUserSummary source) {
        InstagramFollowedUser instagramFollowedUser = new InstagramFollowedUser();

        instagramFollowedUser.setPk(source.getPk());
        instagramFollowedUser.setFullName(source.getFull_name());
        instagramFollowedUser.setProfilePicUrl(source.getProfile_pic_url());
        instagramFollowedUser.setUsername(source.getUsername());
        instagramFollowedUser.setSyncDate(Instant.now());

        return instagramFollowedUser;
    }
}
