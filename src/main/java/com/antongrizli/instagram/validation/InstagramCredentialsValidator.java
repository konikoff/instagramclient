package com.antongrizli.instagram.validation;


import com.antongrizli.instagram.commands.InstagramCredentialsCommand;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class InstagramCredentialsValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return InstagramCredentialsCommand.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "message.login", "Login is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "message.password", "Password is required.");
    }
}
