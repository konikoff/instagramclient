package com.antongrizli.instagram.bootstrap;

import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import com.antongrizli.instagram.services.TaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.List;

@Slf4j
@Component
public class DestroyRunningJobs {

    private final TaskService taskService;

    public DestroyRunningJobs(TaskService taskService) {
        this.taskService = taskService;
    }

    @PreDestroy
    public void gotShutdownSignal() {
        List<Task> runningTasks = taskService.getRunningTasks();
        log.debug("Retrieved " + runningTasks.size() + " tasks.");

        runningTasks.forEach(task -> task.setStatus(TaskStatus.WAITING));

        taskService.saveAllTasks(runningTasks);

        log.debug("Replaced status of tasks and saved!");
    }
}
