package com.antongrizli.instagram.bootstrap;

import com.antongrizli.instagram.domain.Authorities;
import com.antongrizli.instagram.domain.enums.Role;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@Profile("load-data")
public class DataLoader implements CommandLineRunner {

    private final UserService userService;

    public DataLoader(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {
        Authorities userRole = new Authorities();
        userRole.setRole(Role.USER);

        Authorities adminRole = new Authorities();
        adminRole.setRole(Role.ADMIN);

        List<User> users = new ArrayList<>();
        User admin = new User();
        admin.setEmail("admin");
        admin.setPassword("admin");
        admin.assignRole(adminRole);

        users.add(admin);

        User user = new User();
        user.setEmail("user");
        user.setPassword("user");
        user.assignRole(userRole);

        users.add(user);

        for (User one : users) {
            userService.save(one);
            log.debug("Loaded user: " + one.getEmail());
        }
    }
}
