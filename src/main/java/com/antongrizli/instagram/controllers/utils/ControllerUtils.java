package com.antongrizli.instagram.controllers.utils;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.UserConvertToUserCommand;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class ControllerUtils {

    private final UserService userUserService;
    private final UserConvertToUserCommand userConvertToUserCommand;

    public ControllerUtils(UserService userUserService,
                           UserConvertToUserCommand userConvertToUserCommand) {
        this.userUserService = userUserService;
        this.userConvertToUserCommand = userConvertToUserCommand;
    }

    @Deprecated
    public UserCommand getUserCommandByAuthentication(final Authentication authentication) {
        final String login = authentication.getName();

        User savedUser = userUserService.findByLogin(login);
        return userConvertToUserCommand.convert(savedUser);
    }

    public User getUserByAuthentication(final Authentication authentication) {
        final String login = authentication.getName();

        return userUserService.findByLogin(login);
    }

    public UserCommand getConvertToUserCommand(User user) {
        return userConvertToUserCommand.convert(user);
    }

}
