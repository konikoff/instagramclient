package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.controllers.utils.ControllerUtils;
import com.antongrizli.instagram.converters.InstagramFeedResultToPostCommand;
import com.antongrizli.instagram.converters.InstagramFollowedUserToFollowerUserCommand;
import com.antongrizli.instagram.converters.InstagramUserConvertToCommand;
import com.antongrizli.instagram.domain.InstagramFollowerUser;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.services.InstagramFollowerUserService;
import com.antongrizli.instagram.services.InstagramInstanceService;
import com.antongrizli.instagram.services.InstagramService;
import lombok.extern.slf4j.Slf4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedResult;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("instagram")
public class UserController {

    private final InstagramUserConvertToCommand instagramUserConvertToCommand;
    private final InstagramFollowedUserToFollowerUserCommand instagramFollowedUserToFollowerUserCommand;
    private final InstagramFeedResultToPostCommand instagramFeedResultToPostCommand;
    private final InstagramService instagramService;
    private final InstagramInstanceService instagramInstanceService;
    private final InstagramFollowerUserService instagramFollowerUserService;
    private final ControllerUtils controllerUtils;

    public UserController(InstagramUserConvertToCommand instagramUserConvertToCommand,
                          InstagramFollowedUserToFollowerUserCommand instagramFollowedUserToFollowerUserCommand,
                          InstagramFeedResultToPostCommand instagramFeedResultToPostCommand,
                          InstagramService instagramService,
                          InstagramInstanceService instagramInstanceService,
                          InstagramFollowerUserService instagramFollowerUserService,
                          ControllerUtils controllerUtils) {
        this.instagramUserConvertToCommand = instagramUserConvertToCommand;
        this.instagramFollowedUserToFollowerUserCommand = instagramFollowedUserToFollowerUserCommand;
        this.instagramFeedResultToPostCommand = instagramFeedResultToPostCommand;
        this.instagramService = instagramService;
        this.instagramInstanceService = instagramInstanceService;
        this.instagramFollowerUserService = instagramFollowerUserService;
        this.controllerUtils = controllerUtils;
    }

    @GetMapping(value = "/{instagramLogin}")
    public String getInstagramAccount(@PathVariable(value = "instagramLogin") String instagramLogin,
                                      Model model,
                                      Authentication authentication) {

        User savedUser = controllerUtils.getUserByAuthentication(authentication);

        model.addAttribute("instagramAccount", instagramUserConvertToCommand.convert(this.instagramService.getUsernameRequest(savedUser, instagramLogin).getUser()));
        model.addAttribute("userCommand", controllerUtils.getConvertToUserCommand(savedUser));
        return "instagram/account";
    }

    /**
     * @param instagramLogin
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping(value = "/{instagramLogin}/posts")
    public String getPostsForCurrentUser(@PathVariable(value = "instagramLogin") String instagramLogin,
                                         @RequestParam(value = "next_max_id", required = false) String nextMaxId,
                                         Model model,
                                         Authentication authentication) {

        User savedUser = controllerUtils.getUserByAuthentication(authentication);

        InstagramFeedResult feedResult = this.instagramService.getPostsRequest(savedUser, instagramLogin, nextMaxId);
        model.addAttribute("postCommandList", instagramFeedResultToPostCommand.convert(feedResult));
        model.addAttribute("instagramAccount", instagramUserConvertToCommand.convert(this.instagramService.getUsernameRequest(savedUser, instagramLogin).getUser()));
        model.addAttribute("next_max_id", feedResult.getNext_max_id());
        return "instagram/posts";
    }

    /**
     * TODO: Rework the method to use Pageable result
     *
     * @param instagramLogin
     * @param model
     * @param authentication
     * @return
     */
    @GetMapping(value = "/{instagramLogin}/followers")
    public String getFollowersForCurrentUser(@PathVariable(value = "instagramLogin") String instagramLogin,
                                             Model model,
                                             Authentication authentication) {
        UserCommand savedUserCommand = controllerUtils.getUserCommandByAuthentication(authentication);

        List<InstagramFollowerUser> savedFollowedUserList = (List<InstagramFollowerUser>) instagramFollowerUserService.getFollowedUsers(instagramLogin, savedUserCommand);

        model.addAttribute("instagramId", instagramLogin);
        model.addAttribute("followerUsers", instagramFollowedUserToFollowerUserCommand.convertLists(savedFollowedUserList));
        model.addAttribute("userCommand", savedUserCommand);
        return "instagram/follower_list";
    }

    /**
     * TODO: Rework the method to use Pageable result
     *
     * @param instagramLogin - username of instagram account
     * @param model
     * @param authentication
     * @return - view
     */
    @GetMapping(value = "/{instagramLogin}/following")
    public String getFollowingForCurrentUser(@PathVariable(value = "instagramLogin") String instagramLogin,
                                             Model model,
                                             Authentication authentication) {
        UserCommand savedUserCommand = controllerUtils.getUserCommandByAuthentication(authentication);

        List<InstagramFollowerUser> savedFollowingUserList = (List<InstagramFollowerUser>) instagramFollowerUserService.getFollowingUsers(instagramLogin, savedUserCommand);

        model.addAttribute("instagramId", instagramLogin);
        model.addAttribute("followerUsers", instagramFollowedUserToFollowerUserCommand.convertLists(savedFollowingUserList));
        model.addAttribute("userCommand", savedUserCommand);
        return "instagram/follower_list";
    }

    /**
     * Return a list with followed users which follow back
     * TODO: Rework the method to use Pageable result
     *
     * @param instagramLogin - username of instagram account
     * @param model
     * @param authentication
     * @return - view
     */
    @GetMapping("/{instagramLogin}/mutual")
    public String getMutualFollowedUsers(@PathVariable(value = "instagramLogin") String instagramLogin,
                                         Model model,
                                         Authentication authentication) {
        User savedUser = controllerUtils.getUserByAuthentication(authentication);

        List<InstagramFollowerUser> foundMutualUserList = (List<InstagramFollowerUser>) instagramFollowerUserService.getMutualUsers(instagramLogin, savedUser);

        model.addAttribute("instagramLogin", instagramLogin);
        model.addAttribute("followerUsers", instagramFollowedUserToFollowerUserCommand.convertLists(foundMutualUserList));
        return "instagram/follower_list";
    }

    /**
     * Return a list with followed users which don't follow back
     * TODO: Rework the method to use Pageable result
     *
     * @param instagramLogin - username of instagram account
     * @param model
     * @param authentication
     * @return - view
     */
    @GetMapping("/{instagramLogin}/unmutual")
    public String getUnmutualFollowedUsers(@PathVariable(value = "instagramLogin") String instagramLogin,
                                           Model model,
                                           Authentication authentication) {
        User savedUser = controllerUtils.getUserByAuthentication(authentication);

        List<InstagramFollowerUser> foundUnmutualUserList = (List<InstagramFollowerUser>) instagramFollowerUserService.getUnmutualUsers(instagramLogin, savedUser);

        model.addAttribute("instagramId", instagramLogin);
        model.addAttribute("followerUsers", instagramFollowedUserToFollowerUserCommand.convertLists(foundUnmutualUserList));
        return "instagram/follower_list";
    }

    /**
     * Start sync task of followed and following users by demand
     *
     * @param instagramLogin - username of instagram account
     * @param request
     * @param authentication
     * @return - view
     */
    @PostMapping("/{instagramLogin}/sync")
    public String getSyncFollowedUsers(@PathVariable(value = "instagramLogin") String instagramLogin,
                                       HttpServletRequest request,
                                       Authentication authentication) {
        UserCommand savedUserCommand = controllerUtils.getUserCommandByAuthentication(authentication);

        instagramFollowerUserService.syncFollowedUsers(instagramLogin, savedUserCommand);

        instagramFollowerUserService.syncFollowingUsers(instagramLogin, savedUserCommand);
        String referer = request.getHeader("Referer");

        if (referer != null && !referer.isEmpty()) {
            return "redirect:" + referer;
        } else {
            return "redirect:instagram/" + instagramLogin;
        }
    }

    @PostMapping("/{instagramLogin}/unfollow")
    public String unfollowByInstagramUser(@PathVariable(value = "instagramLogin") String instagramLogin,
                                          @RequestParam("instagram_pk") Long pk,
                                          HttpServletRequest request,
                                          Authentication authentication) {

        //instagramFollowerUserService.
        User savedUser = controllerUtils.getUserByAuthentication(authentication);

        instagramFollowerUserService.unfollowInstagramUser(instagramLogin, savedUser, pk);

        String referer = request.getHeader("Referer");
        if (referer != null && !referer.isEmpty()) {
            return "redirect:" + referer;
        } else {
            return "redirect:instagram/" + instagramLogin;
        }
    }
}
