package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.UserConvertToUserCommand;
import com.antongrizli.instagram.commands.InstagramCredentialsCommand;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.services.InstagramAuthService;
import com.antongrizli.instagram.services.InstagramInstanceService;
import com.antongrizli.instagram.services.UserService;
import com.antongrizli.instagram.validation.InstagramCredentialsValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("instagram")
public class InstagramController {

    private static final String SHOW_ALL_URL = "/instagram/show-all";

    private final UserService userService;
    private final InstagramAuthService authService;
    private final InstagramInstanceService instanceService;
    private final UserConvertToUserCommand userConvertToCommand;
    private final InstagramCredentialsValidator instagramCredentialsValidator;

    public InstagramController(UserService userService,
                               InstagramAuthService authService,
                               InstagramInstanceService instanceService,
                               UserConvertToUserCommand userConvertToCommand,
                               InstagramCredentialsValidator instagramCredentialsValidator) {
        this.userService = userService;
        this.authService = authService;
        this.instanceService = instanceService;
        this.userConvertToCommand = userConvertToCommand;
        this.instagramCredentialsValidator = instagramCredentialsValidator;
    }

    @InitBinder("instagramCredentials")
    protected void initInstagramCredentialsBinder(WebDataBinder binder) {
        binder.setValidator(instagramCredentialsValidator);
    }

    @ModelAttribute("instagramCredentials")
    public InstagramCredentialsCommand getInstagramCredentials() {
        return new InstagramCredentialsCommand();
    }

    /**
     * Receiving POST request with Instagram username and password
     *
     * @param credentials - DTO object
     * @return - redirect after authenticate instagram user
     */
    @PostMapping("/auth/login")
    public String loginToInstagram(@ModelAttribute("instagramCredentials") @Valid final InstagramCredentialsCommand credentials,
                                   final BindingResult result,
                                   RedirectAttributes attr,
                                   Authentication authentication) {

        final String login = authentication.getName();

        if (!result.hasErrors()) {
            User savedUser = userService.findByLogin(login);
            UserCommand savedUserCommand = userConvertToCommand.convert(savedUser);

            new Thread(() -> this.authService.login(credentials, savedUserCommand)).start();
        } else {
            List<ObjectError> errors = result.getAllErrors();
            for (ObjectError error : errors) {
                log.debug("This is the error: " + error);
            }
            attr.addFlashAttribute("org.springframework.validation.BindingResult.instagramCredentials", result);
            attr.addFlashAttribute("instagramCredentials", credentials);
        }

        return "redirect:" + SHOW_ALL_URL;
    }

    /**
     * This a temporary method to see all logged accounts.
     * A little bit later will be changed to show accounts by logged user id.
     */
    @GetMapping("/show-all")
    public String showAllLogged(final Model model,
                                Authentication authentication) {

        final String login = authentication.getName();

        User savedUser = userService.findByLogin(login);
        UserCommand userCommand = userConvertToCommand.convert(savedUser);

        model.addAttribute("instagramInstanceList", instanceService.getAllByUserId(userCommand));
        model.addAttribute("userCommand", userCommand);
        return "instagram/show_all";
    }

    @GetMapping("/auth/logout/{id}")
    public String logOut(@PathVariable("id") String id) {

        instanceService.delete(id);
        return "redirect:" + SHOW_ALL_URL;
    }
}
