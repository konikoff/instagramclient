package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.UserCommand;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String getLoginPage(Model model){

        model.addAttribute("user", new Object()); //FIXME: add user object
        return "username";
    }

    @PostMapping("/login")
    public String getLoginPage(@ModelAttribute("user") UserCommand userCommand){
        return "redirect:/";
    }
}
