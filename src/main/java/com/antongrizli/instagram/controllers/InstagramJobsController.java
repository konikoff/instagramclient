package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.IntsagramJobCommand;
import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.UserConvertToUserCommand;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.services.InstagramService;
import com.antongrizli.instagram.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@RequestMapping("instagram")
public class InstagramJobsController {

    private final InstagramService instagramService;
    private final UserConvertToUserCommand userConvertToCommand;
    private final UserService userService;

    public InstagramJobsController(InstagramService instagramService, UserConvertToUserCommand userConvertToCommand, UserService userService) {
        this.instagramService = instagramService;
        this.userConvertToCommand = userConvertToCommand;
        this.userService = userService;
    }

    @GetMapping("/{insta_login}/job/liking")
    public String getLikingJobPage(@PathVariable(value = "insta_login") String instaLogin,
                                   Model model,
                                   Authentication authentication) {

        final String pageTitle = "liking";
        IntsagramJobCommand command = new IntsagramJobCommand();
        command.setPageTitle(pageTitle);

        for (int i = 0; i < 10; i++) {
            command.getLocations().add("");
            command.getTags().add("");
        }

        final String login = authentication.getName();

        model.addAttribute("command", command);
        model.addAttribute("login", login);
        model.addAttribute("insta_login", instaLogin);
        return "instagram/job_form";
    }

    @PostMapping("/{insta_login}/job/liking")
    public String executeLikingJobPage(@PathVariable(value = "insta_login") String instaLogin,
                                       @ModelAttribute("command") IntsagramJobCommand command,
                                       Model model,
                                       Authentication authentication) {

        final String login = authentication.getName();

        User savedUser = this.userService.findByLogin(login);
        UserCommand savedUserCommand = userConvertToCommand.convert(savedUser);

        Thread likingThread = new Thread(() -> this.instagramService.executeTagRequest(savedUserCommand, instaLogin, command));
        likingThread.setName("LikingJobThread");
        likingThread.start();


        return "redirect:/instagram/show-all";
    }
}
