package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.RegistrationFormCommand;
import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("auth")
public class AuthorizationController {

    private final UserService userService;

    public AuthorizationController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("registrationForm")
    public RegistrationFormCommand getRegistrationFormCommand() {
        return new RegistrationFormCommand();
    }

    @GetMapping("/sign-in")
    public String getLoginPage(Model model) {

        model.addAttribute("loginUser", new UserCommand());
        return "fragments/login";
    }

    @PostMapping("/sign-in")
    public String executeLogin(HttpServletRequest request, @ModelAttribute("loginUser") UserCommand loginUser) {

        return "redirect:" + request.getHeader("Referer");
    }

    @GetMapping("/sign-up")
    public String getSignUpPage(Model model) {

        return "fragments/sign_up";
    }

    @PostMapping("/sign-up")
    public String saveSignUp(HttpServletRequest request,
                             @ModelAttribute("registrationForm") @Valid final RegistrationFormCommand registrationForm,
                             final BindingResult result,
                             Model model) {

        if (!result.hasErrors()) {
            userService.registerNewUserAccount(registrationForm);

            return "redirect:" + request.getHeader("Referer");
        } else {
            return "fragments/sign_up";
        }

    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Model model) {

        return "redirect:" + request.getHeader("Referer");
    }
}
