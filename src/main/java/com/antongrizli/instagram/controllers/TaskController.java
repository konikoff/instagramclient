package com.antongrizli.instagram.controllers;

import com.antongrizli.instagram.commands.TaskCommand;
import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.TaskCommandConvertToTask;
import com.antongrizli.instagram.converters.TaskConvertToTaskCommand;
import com.antongrizli.instagram.converters.UserConvertToUserCommand;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.User;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import com.antongrizli.instagram.services.TaskService;
import com.antongrizli.instagram.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("instagram")
public class TaskController {

    private final TaskService taskService;
    private final TaskCommandConvertToTask taskCommandConvertToTask;
    private final TaskConvertToTaskCommand taskConvertToTaskCommand;
    private final UserService userService;
    private final UserConvertToUserCommand userConvertToCommand;

    public TaskController(TaskService taskService,
                          TaskCommandConvertToTask taskCommandConvertToTask, TaskConvertToTaskCommand taskConvertToTaskCommand, UserService userService,
                          UserConvertToUserCommand userConvertToCommand) {
        this.taskService = taskService;
        this.taskCommandConvertToTask = taskCommandConvertToTask;
        this.taskConvertToTaskCommand = taskConvertToTaskCommand;
        this.userService = userService;
        this.userConvertToCommand = userConvertToCommand;
    }

    @GetMapping("/{insta_login}/task")
    public String showAllTasks(@PathVariable("insta_login") String instagramLogin,
                               Model model,
                               Authentication authentication) {
        final String login = authentication.getName();

        UserCommand savedUserCommand = getSavedUserCommand(login);

        model.addAttribute("userCommand", savedUserCommand);
        model.addAttribute("instagramLogin", instagramLogin);

        List<Task> taskList = taskService.getAllTasksByInstagramLogin(savedUserCommand, instagramLogin);
        List<TaskCommand> taskCommandList = new ArrayList<>();
        for (Task task : taskList) {
            taskCommandList.add(taskConvertToTaskCommand.convert(task));
        }

        model.addAttribute("taskCommandList", taskCommandList);
        return "instagram/tasks/show_all";
    }

    @GetMapping("/{insta_login}/task/{task_id}")
    public String showTask(@PathVariable("insta_login") String instagramLogin,
                           @PathVariable("task_id") Long taskId,
                           Model model,
                           Authentication authentication) {
        final String login = authentication.getName();

        UserCommand savedUserCommand = getSavedUserCommand(login);

        for (InstagramCredentials credential : savedUserCommand.getInstagramCredentials()) {
            if (instagramLogin.equalsIgnoreCase(credential.getUsername())) {
                log.debug("Show task");

                Task savedTask = taskService.findTaskByIdAndCredential(taskId, credential);
                TaskCommand taskCommand = taskConvertToTaskCommand.convert(savedTask);

                model.addAttribute("userCommand", savedUserCommand);
                model.addAttribute("instagramLogin", instagramLogin);
                model.addAttribute("taskCommand", taskCommand);
                return "instagram/tasks/show_task";
            }
        }

        throw new RuntimeException("Unfortunately this user: " + instagramLogin + " doesn't belong to your account!");
    }

    @GetMapping("/{insta_login}/task/new")
    public String createTask(@PathVariable("insta_login") String instagramLogin,
                             Model model,
                             Authentication authentication) {
        final String login = authentication.getName();

        UserCommand savedUserCommand = getSavedUserCommand(login);

        model.addAttribute("userCommand", savedUserCommand);
        model.addAttribute("instagramLogin", instagramLogin);
        model.addAttribute("taskCommand", new TaskCommand());
        return "instagram/tasks/show_task";
    }

    @PostMapping("/{insta_login}/task")
    public String performCreateTask(@PathVariable("insta_login") String instagramLogin,
                                    TaskCommand taskCommand,
                                    Authentication authentication) {
        final String login = authentication.getName();

        UserCommand savedUserCommand = getSavedUserCommand(login);

        for (InstagramCredentials credential : savedUserCommand.getInstagramCredentials()) {
            if (instagramLogin.equalsIgnoreCase(credential.getUsername())) {
                String savedInstagramLogin = credential.getUsername();

                taskCommand.setStatus(TaskStatus.WAITING);
                Task task = taskCommandConvertToTask.convert(taskCommand);
                task.setInstagramCredentials(credential);

                Task savedTask = taskService.save(task);

                Long taskId = savedTask.getId();

                return "redirect:/instagram/" + savedInstagramLogin + "/task/" + taskId;
            }
        }

        throw new RuntimeException("Unfortunately this user: " + instagramLogin + " doesn't belong to your account!");
    }

    @GetMapping("/{insta_login}/task/{task_id}/delete")
    public String deleteTask(@PathVariable("insta_login") String instagramLogin,
                             @PathVariable("task_id") Long taskId,
                             Authentication authentication) {
        final String login = authentication.getName();

        UserCommand savedUserCommand = getSavedUserCommand(login);

        for (InstagramCredentials credential : savedUserCommand.getInstagramCredentials()) {
            if (instagramLogin.equalsIgnoreCase(credential.getUsername())) {
                taskService.deleteTaskByIdAndCredential(taskId, credential);

                return "redirect:/instagram/" + instagramLogin + "/task";
            }
        }
        throw new RuntimeException("Unfortunately this user: " + instagramLogin + " doesn't belong to your account!");
    }

    private UserCommand getSavedUserCommand(String login) {
        User savedUser = this.userService.findByLogin(login);
        return userConvertToCommand.convert(savedUser);
    }
}
