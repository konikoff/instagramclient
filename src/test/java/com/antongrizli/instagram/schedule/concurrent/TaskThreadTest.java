package com.antongrizli.instagram.schedule.concurrent;

import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

class TaskThreadTest {

    List<Task> tasks;

    @BeforeEach
    void setUp() {
        tasks = new ArrayList<>();

        Task task1 = new Task();
        task1.setTaskName("Follow");
        InstagramCredentials credentials1 = new InstagramCredentials();
        credentials1.setId(1L);
        task1.setInstagramCredentials(credentials1);

        tasks.add(task1);

        Task task2 = new Task();
        task2.setTaskName("UnFollow");
        InstagramCredentials credentials2 = new InstagramCredentials();
        credentials2.setId(2L);
        task2.setInstagramCredentials(credentials2);

        tasks.add(task2);

        Task task3 = new Task();
        task3.setTaskName("Like");
        InstagramCredentials credentials3 = new InstagramCredentials();
        credentials3.setId(3L);
        task3.setInstagramCredentials(credentials3);

        tasks.add(task3);

        Task task4 = new Task();
        task4.setTaskName("All");
        task4.setInstagramCredentials(credentials3);

        tasks.add(task4);
    }

    @Test
    void testTaskConcurrently() {
        Map<Long, List<Task>> taskMapByCredential = new HashMap<>();

        for (Task task : tasks) {
            Long credentialsId = task.getInstagramCredentials().getId();
            if (taskMapByCredential.containsKey(credentialsId)) {
                taskMapByCredential.get(credentialsId).add(task);
            } else {
                taskMapByCredential.put(credentialsId, new ArrayList<Task>() {{
                    add(task);
                }});
            }
        }

        for (Map.Entry<Long, List<Task>> taskEntry : taskMapByCredential.entrySet()) {
            for (Task task : taskEntry.getValue()) {
                TaskThread taskThread = new TaskThread(null, null, null);
                new Thread(taskThread).start();
            }
        }
    }

    public static void main(String[] args) {
        TaskThreadTest test = new TaskThreadTest();

        test.setUp();

        test.testTaskConcurrently();
    }
}