package com.antongrizli.instagram.schedule;

import com.antongrizli.instagram.converters.InstagramUserSummaryToInstagramUser;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.services.InstagramCredentialService;
import com.antongrizli.instagram.services.InstagramFollowerUserService;
import com.antongrizli.instagram.services.InstagramInstanceService;
import com.antongrizli.instagram.services.InstagramService;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.payload.InstagramGetUserFollowersResult;
import org.brunocvcunha.instagram4j.requests.payload.InstagramLoginResult;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SyncFollowersTest {

    SyncFollowers syncFollowers;

    @Mock
    InstagramCredentialService credentialService;

    @Mock
    InstagramInstanceService instanceService;

    @Mock
    InstagramService instagramService;

    @Mock
    InstagramFollowerUserService instagramFollowedUserService;
    @Mock
    InstagramUserSummaryToInstagramUser userSummaryToInstagramUser;

    @Mock
    Instagram4j instagram4j;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.syncFollowers = new SyncFollowers(credentialService, instanceService, userSummaryToInstagramUser, instagramService, instagramFollowedUserService);
    }

    @Test
    void syncFollowers() throws Exception {
        //given
        List<InstagramCredentials> credentialsList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            InstagramCredentials credentials = new InstagramCredentials();

            credentialsList.add(credentials);
        }
        //when
        when(credentialService.getAllCredentials()).thenReturn(credentialsList);

        instagram4j = new Instagram4j("username", "password");
        when(instanceService.getInstagram4j(any())).thenReturn(instagram4j);


        this.instagram4j = mock(Instagram4j.class);

        when(instagram4j.login()).thenReturn(new InstagramLoginResult());
        instagram4j.login();

        InstagramGetUserFollowersResult result = new InstagramGetUserFollowersResult();
        List<InstagramUserSummary> userSummaryList = new ArrayList<>();
        result.setUsers(userSummaryList);
        when(instagram4j.sendRequest(any())).thenReturn(result);

        doNothing().when(credentialService).save(any());
        //then
    }
}