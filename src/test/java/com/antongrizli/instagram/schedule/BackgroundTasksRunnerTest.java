package com.antongrizli.instagram.schedule;

import com.antongrizli.instagram.domain.Task;
import com.antongrizli.instagram.domain.enums.TaskAction;
import com.antongrizli.instagram.domain.enums.TaskStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static org.junit.jupiter.api.Assertions.*;

class BackgroundTasksRunnerTest {

    static final int CAPCITY = 10;
    BlockingQueue<Task> blockingQueue = new ArrayBlockingQueue<>(CAPCITY);

    private Map<Long, List<Task>> taskMap = new HashMap<>();

    @BeforeEach
    void setUp() {
        blockingQueue.clear();

        taskMap.clear();

        List<Task> taskList = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            Task task = new Task();
            task.setId((long) (i + 1));
            task.setStatus(TaskStatus.WAITING);
            task.setTaskName("Task " + (i + 1));
            task.setAction(TaskAction.LIKE);

            System.out.println("Adding the new element: " + task.getTaskName());

            try {
                taskList.add(task);
            } catch (RuntimeException e) {
                System.out.println(e.getCause());
            }
        }

        taskMap.put(1l, taskList);
        taskMap.put(2l, taskList);
    }

    @Test
    void executeTasks() throws InterruptedException {

        Thread fillingInQueue = new Thread(() -> {
            for (Map.Entry<Long, List<Task>> taskEntry : taskMap.entrySet()) {
                for (Task task : taskEntry.getValue()) {
                    try {
                        blockingQueue.put(task);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        fillingInQueue.start();

        Thread executeTasks = new Thread(() -> {
            try {
                while (!blockingQueue.isEmpty()) {
                    Task task = blockingQueue.take();

                    System.out.println("Retrieved " + task.getTaskName());

                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executeTasks.start();


        while (executeTasks.isAlive()) {
            try {
                Thread.sleep(500);
                System.out.println("Sleeping. Queue size: " + blockingQueue.size());
            } catch (InterruptedException ignored) {
            }
        }
    }
}