package com.antongrizli.instagram.services;

import com.antongrizli.instagram.domain.InstagramFollowedUser;
import com.antongrizli.instagram.repositories.InstagramFollowedUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

class InstagramFollowedUserServiceImplTest {

    @Mock
    InstagramFollowedUserRepository instagramFollowedUserRepository;

    InstagramFollowerUserService instagramFollowedUserService;

    List<InstagramFollowedUser> savedFollowedUserList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

     //   this.instagramFollowedUserService = new InstagramFollowedUserServiceImpl(instagramFollowedUserRepository, instanceInstanceService, instagramService, userSummaryToInstagramUser);

        savedFollowedUserList = new ArrayList<>();

        InstagramFollowedUser instagramFollowedUser1 = new InstagramFollowedUser();
        instagramFollowedUser1.setPk(1l);
        instagramFollowedUser1.setUsername("User1");

        savedFollowedUserList.add(instagramFollowedUser1);

        InstagramFollowedUser instagramFollowedUser2 = new InstagramFollowedUser();
        instagramFollowedUser2.setPk(2l);
        instagramFollowedUser2.setUsername("User2");

        savedFollowedUserList.add(instagramFollowedUser2);
    }

    @Test
    void saveFollowers() {
        //given
        List<InstagramFollowedUser> newFollowedUserList = new ArrayList<>();

        InstagramFollowedUser instagramFollowedUser1 = new InstagramFollowedUser();
        instagramFollowedUser1.setPk(1l);
        instagramFollowedUser1.setUsername("User1");

        savedFollowedUserList.add(instagramFollowedUser1);

        InstagramFollowedUser instagramFollowedUser2 = new InstagramFollowedUser();
        instagramFollowedUser2.setPk(2l);
        instagramFollowedUser2.setUsername("User2");

        newFollowedUserList.add(instagramFollowedUser2);

        InstagramFollowedUser instagramFollowedUser3 = new InstagramFollowedUser();
        instagramFollowedUser3.setPk(3l);
        instagramFollowedUser3.setUsername("User3");

        newFollowedUserList.add(instagramFollowedUser3);
        //when
        when(instagramFollowedUserRepository.findAllByInstagramCredentials_Id(anyLong())).thenReturn(Optional.of(savedFollowedUserList));

        doAnswer((Answer<InstagramFollowedUser>) invocation -> {
            InstagramFollowedUser followedUser = invocation.getArgument(0);

            assertEquals(Long.valueOf(3l), followedUser.getPk());
            assertEquals("User3", followedUser.getUsername());

            return null;
        }).when(instagramFollowedUserRepository).save(any());
        //then
        instagramFollowedUserService.saveFollowers(1l, newFollowedUserList);
    }

    @Test
    void deleteFollowersFromTable() {
        //given
        savedFollowedUserList.remove(0);

        List<InstagramFollowedUser> newFollowedUserList = new ArrayList<>();

        InstagramFollowedUser instagramFollowedUser1 = new InstagramFollowedUser();
        instagramFollowedUser1.setPk(1l);
        instagramFollowedUser1.setUsername("User1");

        savedFollowedUserList.add(instagramFollowedUser1);

        InstagramFollowedUser instagramFollowedUser2 = new InstagramFollowedUser();
        instagramFollowedUser2.setPk(2l);
        instagramFollowedUser2.setUsername("User2");

        newFollowedUserList.add(instagramFollowedUser2);

        InstagramFollowedUser instagramFollowedUser3 = new InstagramFollowedUser();
        instagramFollowedUser3.setPk(3l);
        instagramFollowedUser3.setUsername("User3");

        newFollowedUserList.add(instagramFollowedUser3);
        //when
        when(instagramFollowedUserRepository.findAllByInstagramCredentials_Id(anyLong())).thenReturn(Optional.of(savedFollowedUserList));

        doAnswer((Answer<InstagramFollowedUser>) invocation -> {
            InstagramFollowedUser followedUser = invocation.getArgument(0);

            assertEquals(Long.valueOf(1l), followedUser.getPk());
            assertEquals("User1", followedUser.getUsername());

            return null;
        }).when(instagramFollowedUserRepository).delete(any());
        //then
        instagramFollowedUserService.saveFollowers(1l, newFollowedUserList);
    }
}