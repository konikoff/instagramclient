package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.converters.UserConvertToUserCommand;
import com.antongrizli.instagram.domain.InstagramCredentials;
import com.antongrizli.instagram.domain.User;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class InstagramCredentialsServiceImplTest {

    @Autowired
    UserService userService;

    @Autowired
    UserConvertToUserCommand toUserCommand;

    @Autowired
    InstagramInstanceService service;

    @BeforeEach
    void setUp() {
        userService.deleteAll();

        User user = new User();
        user.setEmail("user");
        user.setPassword("password");
        userService.save(user);
    }

    @Test
    void getAllInstagramInstances() {
    }

    @Test
    void save() {
        User savedUser = userService.findByLogin("user");

        assertNotNull(savedUser.getEmail());

        UserCommand userCommand = toUserCommand.convert(savedUser);
        InstagramCredentials savedInstance = service.save(new Instagram4j("ivan", "ivanov"), userCommand);

        assertNotNull(savedInstance);
    }
}