package com.antongrizli.instagram.services;

import com.antongrizli.instagram.commands.UserCommand;
import com.antongrizli.instagram.commands.InstagramCredentialsCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class InstagramAuthServiceImplTest {

    @Autowired
    InstagramAuthService service;

    @BeforeEach
    void setUp() {
    }

    @Test
    void login() {
        InstagramCredentialsCommand credentials = new InstagramCredentialsCommand();
        credentials.setLogin("ivan");
        credentials.setPassword("ivanov");

        service.login(credentials, UserCommand.builder().login("user").password("password").build());
    }

    @Test
    void logout() {
    }
}