package com.antongrizli.instagram.services;

import com.antongrizli.instagram.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith({SpringExtension.class})
class UserServiceImplTest {

    @Autowired
    UserService userService;

    List<User> users;

    @BeforeEach
    void setUp() {
        userService.deleteAll();

        users = new ArrayList<>();

        User user1 = new User();
        user1.setEmail("login1");
        user1.setPassword("password1");
        users.add(user1);

        User user2 = new User();
        user2.setEmail("login2");
        user2.setPassword("password2");
        users.add(user2);

        for (User user : users) {
            userService.save(user);
        }

    }

    @Test
    void save() {
        User user = new User();
        user.setEmail("login3");
        user.setPassword("password3");

        userService.save(user);

        List<User> userCommands = userService.findAll();

        for (User userCommand : userCommands) {
            System.out.println("User username: " + userCommand.getEmail() + ", User password: " + userCommand.getPassword());
        }
        assertEquals(3, userCommands.size());
    }

    @Test
    void saveSame() {
        User user1 = new User();
        user1.setEmail("login1");
        user1.setPassword("password1");

        userService.save(user1);

        List<User> userCommands = userService.findAll();

        for (User user : userCommands) {
            System.out.println(user);
        }
        assertEquals(2, userCommands.size());
    }

}