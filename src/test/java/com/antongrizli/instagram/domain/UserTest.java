package com.antongrizli.instagram.domain;

import com.antongrizli.instagram.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserTest {

    private static final String USER_NAME = "Vasia";

    @Autowired
    UserService userService;


    @BeforeEach
    void setUp() {
        userService.deleteAll();
    }

    @Test
    void saveUserWithFollowersAndFollowings() {
        User user = new User();
        user.setEmail(USER_NAME);
        user.setPassword("Petrov");

        List<InstagramCredentials> credentialsList = new ArrayList<>();
        InstagramCredentials credentials = new InstagramCredentials();
        credentials.setUsername("instagram");
        credentials.setPassword("instaword");
        credentials.setUuid("uuii-uuui");
        credentials.setBytesCookieStore(new Byte[]{100, 111, 122});
        credentials.setInstagramUserId(11199900000l);

        //Followers
        List<InstagramFollowedUser> followers = new ArrayList<>();
        InstagramFollowedUser follower1 = new InstagramFollowedUser();
        follower1.setFullName("Follower1");
        follower1.setPk(9099090l);
        followers.add(follower1);

        InstagramFollowedUser follower2 = new InstagramFollowedUser();
        follower2.setFullName("Follower2");
        follower2.setPk(908889090l);
        followers.add(follower2);

        credentials.setFollowers(followers);

        //Follwoings
        List<InstagramFollowedUser> followings = new ArrayList<>();
        InstagramFollowedUser following1 = new InstagramFollowedUser();
        following1.setFullName("Following1");
        following1.setPk(111099090l);
        followings.add(following1);

        InstagramFollowedUser following2 = new InstagramFollowedUser();
        following2.setFullName("Following2");
        following2.setPk(1121212908889090l);
        followings.add(following2);

       // credentials.setFollowings(followings);

        credentialsList.add(credentials);
        user.setInstagramCredentials(credentialsList);

        userService.save(user);

        User savedUser = userService.findByLogin(USER_NAME);

        assertEquals(USER_NAME, savedUser.getEmail());
        assertEquals(2, savedUser.getInstagramCredentials().get(0).getFollowers().size());
    }
}