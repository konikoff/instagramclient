package com.antongrizli.instagram.domain;

import org.apache.http.client.CookieStore;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.InstagramGetMediaLikersRequest;
import org.brunocvcunha.instagram4j.requests.payload.InstagramGetMediaLikersResult;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;


class InstagramInstanceTest {

    private final static String LOGIN = "LOGIN";
    private final static String PASSWORD = "PASSWORD";

    static Instagram4j instagram;

    static byte[] cookieStoreBytes;

    @BeforeAll
    static void setUp() throws Exception {
        instagram = Instagram4j.builder().username(LOGIN).password(PASSWORD).build();

        instagram.setup();

        instagram.login();
    }

    @Test
    void serializeInstagram4jToByte() throws Exception {
        CookieStore cookieStore = instagram.getCookieStore();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(cookieStore);
            out.flush();

            cookieStoreBytes = bos.toByteArray();

            assertNotNull(cookieStoreBytes);
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    @AfterAll
    static void deserializeInstagram4j() throws Exception {
        ObjectInputStream ios = new ObjectInputStream(new ByteArrayInputStream(cookieStoreBytes));
        CookieStore cookieStore = (CookieStore) ios.readObject();

        Instagram4j instagram4j = Instagram4j.builder()
                .username("random_name")
                .password("random_password")
                .cookieStore(cookieStore).build();
        instagram4j.setup();

        InstagramGetMediaLikersResult tagFeed = instagram4j.sendRequest(new InstagramGetMediaLikersRequest(1020304050L));

        assertNotNull(tagFeed);
    }
}